[Transient Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 1 {524290,0,"V(osc)"}
      X: ('n',0,0,1e-008,1e-007)
      Y[0]: (' ',1,-0.5,0.5,5.5)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,-0.5,0.5,5.5)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {524291,0,"V(osc_pad)"}
      X: ('n',0,0,1e-008,1e-007)
      Y[0]: (' ',1,0,0.5,5.5)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,0,0.5,5.5)
      Log: 0 0 0
      GridStyle: 1
   }
}
