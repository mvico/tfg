# Características de los amplificadores operacionales ideales:

## Impedancia de entrada infinita
Cada una de las entradas de este tipo de amplificador, presenta una muy alta ($inf$ en el caso ideal) impedancia de entrada ($Z_{i}$) eso implica una corriente nula ($i_{i}$) de entrada al operacional, por ende, este tipo de amplificador no cargaría al circuito anterior al que está conectado (desde el que estaría recibiendo la señal a amplificar).

## Ganancia de bucle abierto infinita (para la señal diferencial)
Toda señal que ingrese al amplificador por sus dos entradas serán restadas entre sí (debido al funcionamiento interno del amplificador, para lo cual además no deberá existir ningún lazo de realimentación) y es esta diferencia de señales, quién será amplificada infinitamente y se podrá recuperar en la salida del amplificador.

## Ganancia nula para la señal de modo común
La señal de modo común hace referencia a las componentes de señal que ambas señales de entrada comparten, es por ende esperado de un amplificador diferencial, que solo se amplifique la diferencia de las dos señales presentes, por ende se requiere una ganancia nula para la señal común a ambas entradas.

## Impedancia de salida nula
Concebido como un amplificador de tensión, el amplificador operacional ideal debe presentar una impedancia de salida nula ($Z_{o} = 0 \Ohm$), de esta manera, puede forzar su salida a cualquier nivel de tensión (y corriente) sin que estas dependan de la carga colocada a la salida. De esta manera, el amplificador operacional puede comportarse como una fuente de tensión ideal, controlada por tensión (de entrada).

## Ancho de banda infinito
Se espera de un amplificador que trabaje de igual manera para cualquier frecuencia que ingrese por sus entradas, es decir, se espera que pueda amplificar con la misma ganancia cualquier frecuencia. Obviamente para el caso ideal se tiene un ancho de banda $B = \inf$.
