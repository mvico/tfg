[Noise Spectral Density - (V/Hz� or A/Hz�)]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 1 {524290,0,"V(inoise)"}
      X: ('M',0,1,0,1e+008)
      Y[0]: ('�',0,0,2e-006,1.6e-005)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "V/Hz�" ('�',0,0,0,0,2e-006,1.6e-005)
      Log: 1 0 0
      GridStyle: 1
   },
   {
      traces: 1 {268959747,0,"V(onoise)"}
      X: ('M',0,1,0,1e+008)
      Y[0]: ('�',0,0,3e-005,0.00033)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "V/Hz�" ('�',0,0,0,0,3e-005,0.00033)
      Log: 1 0 0
      GridStyle: 1
   }
}
