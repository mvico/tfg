[Transient Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 1 {524290,0,"V(dout)"}
      X: ('n',0,0,5e-008,5e-007)
      Y[0]: (' ',1,0,0.3,3)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,0,0.3,3)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {524291,0,"V(out_pad)"}
      X: ('n',0,0,5e-008,5e-007)
      Y[0]: (' ',1,-0.3,0.3,3.3)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,-0.3,0.3,3.3)
      Log: 0 0 0
      GridStyle: 1
   }
}
