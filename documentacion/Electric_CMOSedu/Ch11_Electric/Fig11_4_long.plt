[DC transfer characteristic]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 1 {524290,0,"V(vout)"}
      X: (' ',1,0,0.5,5)
      Y[0]: (' ',1,0,0.5,5)
      Y[1]: ('�',0,1e+308,5e-005,-1e+308)
      Volts: (' ',0,0,1,0,0.5,5)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {34603011,0,"Id(Mm1)"}
      X: (' ',1,0,0.5,5)
      Y[0]: ('�',0,0,5e-005,0.00055)
      Y[1]: ('�',0,1e+308,5e-005,-1e+308)
      Amps: ('�',0,0,1,0,5e-005,0.00055)
      Log: 0 0 0
      GridStyle: 1
   }
}
