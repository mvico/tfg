[Transient Analysis]
{
   Npanes: 1
   {
      traces: 4 {524290,0,"V(vip1)"} {524291,0,"V(vip1b)+1.25"} {524292,0,"V(vip2)+2.5"} {524293,0,"V(vip2b)+3.75"}
      X: ('n',0,0,2e-009,2e-008)
      Y[0]: (' ',1,0,0.5,5)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,0,0.5,5)
      Log: 0 0 0
      GridStyle: 1
   }
}
