[Transient Analysis]
{
   Npanes: 3
   Active Pane: 2
   {
      traces: 2 {524290,0,"V(voutp)-V(voutm)"} {524291,0,"(V(vinsp)-V(vinsm))"}
      X: ('n',0,0,5e-008,5e-007)
      Y[0]: ('m',0,-0.3,0.06,0.3)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.3,0.06,0.3)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 2 {524292,0,"V(vopp)"} {524293,0,"V(vopm)"}
      X: ('n',0,0,5e-008,5e-007)
      Y[0]: ('m',0,0,0.06,0.66)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,0,0.06,0.66)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 3 {524294,0,"V(phi1)+2.2"} {524295,0,"V(phi3)"} {524296,0,"V(phi2)+1.1"}
      X: ('n',0,0,5e-008,5e-007)
      Y[0]: (' ',1,0,0.3,3.3)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,0,0.3,3.3)
      Log: 0 0 0
      GridStyle: 1
   }
}
