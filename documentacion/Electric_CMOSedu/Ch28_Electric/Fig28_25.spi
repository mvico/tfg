*** SPICE deck for cell Fig28_25{sch} from library Ch28_Electric
*** Created on Tue Apr 29, 2008 18:54:06
*** Last revised on Sat Oct 09, 2010 19:20:44
*** Written on Sun Nov 21, 2010 19:11:33 by Electric VLSI Design System, 
*version 9.00-q
*** Layout tech: mocmos, foundry MOSIS
*** UC SPICE *** , MIN_RESIST 4.0, MIN_CAPAC 0.1FF

*** SUBCIRCUIT Ideal_Switch FROM CELL Ideal_Switch{sch}
.SUBCKT Ideal_Switch contrlm contrlp in out

* Spice Code nodes in cell cell 'Ideal_Switch{sch}'
Sswitch  in out  contrlp contrlm  switmod
.model switmod sw
.ENDS Ideal_Switch

*** SUBCIRCUIT Ideal_DACbit FROM CELL Ideal_DACbit{sch}
.SUBCKT Ideal_DACbit Bitin Bitout Vone Vtrip
** GLOBAL gnd
Xideal_sw@0 Bitin Vtrip Bitout gnd Ideal_Switch
Xideal_sw@1 Vtrip Bitin Vone Bitout Ideal_Switch
.ENDS Ideal_DACbit

*** SUBCIRCUIT Ideal_10Bit_DAC FROM CELL Ideal_10Bit_DAC{sch}
.SUBCKT Ideal_10Bit_DAC B0 B1 B2 B3 B4 B5 B6 B7 B8 B9 VREFM VREFP Vout vdd
** GLOBAL gnd
Rres@0 vdd Vtrip 100000k
Rres@1 Vtrip gnd 100000k
Rres@2 VREFP VREFM 100000k
XIdeal_DA@0 B6 B6L Vone Vtrip Ideal_DACbit
XIdeal_DA@1 B2 B2L Vone Vtrip Ideal_DACbit
XIdeal_DA@2 B4 B4L Vone Vtrip Ideal_DACbit
XIdeal_DA@3 B0 B0L Vone Vtrip Ideal_DACbit
XIdeal_DA@4 B5 B5L Vone Vtrip Ideal_DACbit
XIdeal_DA@5 B1 B1L Vone Vtrip Ideal_DACbit
XIdeal_DA@6 B7 B7L Vone Vtrip Ideal_DACbit
XIdeal_DA@7 B3 B3L Vone Vtrip Ideal_DACbit
XIdeal_DA@8 B9 B9L Vone Vtrip Ideal_DACbit
XIdeal_DA@9 B8 B8L Vone Vtrip Ideal_DACbit

* Spice Code nodes in cell cell 'Ideal_10Bit_DAC{sch}'
Vone Vone 0 DC 1
B1 Vout 0 
+V=((v(vrefp)-v(vrefm))/1024)*(v(B9L)*512+v(B8L)*256+v(B7L)*128+v(B6L)*64+v(B5L)*32+v(B4L)*16+v(B3L)*8+v(B2L)*4+v(B1L)*2+v(B0L))+v(vrefm)
.ENDS Ideal_10Bit_DAC

*** SUBCIRCUIT Ideal_Differencer_2 FROM CELL Ideal_Differencer_2{sch}
.SUBCKT Ideal_Differencer_2 im ip om op

* Spice Code nodes in cell cell 'Ideal_Differencer_2{sch}'
E  op  om  ip  im  2
.ENDS Ideal_Differencer_2

*** SUBCIRCUIT Ideal_ADCbit FROM CELL Ideal_ADCbit{sch}
.SUBCKT Ideal_ADCbit Bitout VCM Vin Vout Vtrip vdd
** GLOBAL gnd
XIdeal_sw@0 VCM Vin vdd Bitout Ideal_Switch
XIdeal_sw@1 Vin VCM Bitout gnd Ideal_Switch
XIdeal_sw@4 Vtrip Bitout Vinh Vout Ideal_Switch
XIdeal_sw@5 Bitout Vtrip Vout Vinl Ideal_Switch
Xideal_co@0 VCM Vin gnd Vinh Ideal_Differencer_2
Xideal_co@1 gnd Vin gnd Vinl Ideal_Differencer_2
.ENDS Ideal_ADCbit

*** SUBCIRCUIT Ideal_Opamp FROM CELL Ideal_Opamp{sch}
.SUBCKT Ideal_Opamp Vm Vo Vp
** GLOBAL gnd
Rres@0 Vp Vm 1410.065meg
Rres@1 Vo gnd 1

* Spice Code nodes in cell cell 'Ideal_Opamp{sch}'
G1 Vo 0  Vp  Vm 100MEG
.ENDS Ideal_Opamp

*** SUBCIRCUIT Ideal_Sample_Hold FROM CELL Ideal_Sample&Hold{sch}
.SUBCKT Ideal_Sample_Hold Vclk Vin Voutsh vdd
** GLOBAL gnd
Ccap@0 gnd Vins 0.1n
Ccap@1 gnd net@32 0.1f
Rres@0 vdd Vtrip 100000k
Rres@1 Vtrip gnd 100000k
XIdeal_sw@0 Vclk Vtrip Vinb Vins Ideal_Switch
XIdeal_sw@1 Vtrip Vclk Vins net@32 Ideal_Switch
Xideal_op@0 Voutsh Voutsh net@32 Ideal_Opamp
Xideal_op@1 Vinb Vinb Vin Ideal_Opamp
.ENDS Ideal_Sample_Hold

*** SUBCIRCUIT Ideal_10Bit_ADC FROM CELL Ideal_10Bit_ADC{sch}
.SUBCKT Ideal_10Bit_ADC B0 B1 B2 B3 B4 B5 B6 B7 B8 B9 VREFM VREFP Vin clock 
+vdd
** GLOBAL gnd
Rres@0 vdd Vtrip 100000k
Rres@1 Vtrip gnd 100000k
X_18_Ideal@0 B6 VCM net@57 net@25 Vtrip vdd Ideal_ADCbit
X_18_Ideal@1 B5 VCM net@25 net@24 Vtrip vdd Ideal_ADCbit
X_18_Ideal@6 B4 VCM net@24 net@18 Vtrip vdd Ideal_ADCbit
X_18_Ideal@8 B2 VCM net@51 net@16 Vtrip vdd Ideal_ADCbit
X_18_Ideal@9 B1 VCM net@16 net@15 Vtrip vdd Ideal_ADCbit
X_18_Ideal@10 B0 VCM net@15 _18_Ideal@10_Vout Vtrip vdd Ideal_ADCbit
XIdeal_AD@0 B7 VCM net@60 net@57 Vtrip vdd Ideal_ADCbit
XIdeal_AD@1 B3 VCM net@18 net@51 Vtrip vdd Ideal_ADCbit
XIdeal_AD@2 B9 VCM PIPIN net@59 Vtrip vdd Ideal_ADCbit
XIdeal_AD@3 B8 VCM net@59 net@60 Vtrip vdd Ideal_ADCbit
Xideal_sa@0 clock Vin VOUTSH vdd Ideal_Sample_Hold

* Spice Code nodes in cell cell 'Ideal_10Bit_ADC{sch}'
B1 VCM 0 V=(V(VREFP)-V(VREFM))/2
BPIP PIPIN 0 V=V(VOUTSH)-V(VREFM)+((V(VREFP)-V(VREFM))/2048)
.ENDS Ideal_10Bit_ADC

.global gnd vdd

*** TOP LEVEL CELL: Fig28_25{sch}
XIdeal_3B@0 net@65 net@64 net@63 net@62 net@61 net@60 net@59 net@58 net@57 
+net@56 gnd vdd Vout vdd Ideal_10Bit_DAC
XIdeal_3B@1 net@65 net@64 net@63 net@62 net@61 net@60 net@59 net@58 net@57 
+net@56 gnd vdd Vin Vclock vdd Ideal_10Bit_ADC

* Spice Code nodes in cell cell 'Fig28_25{sch}'
VDD VDD 0 DC 1
VGND GND 0 DC 0
Vin  Vin  0 DC 0 SINE(0.5 0.5 60MEG 0)
Vclock  Vclock  0 DC 0 PULSE(0 1 5n 100p 100p 4.9n 10n)
.tran 10p 200n 10p .1n uic
*.options post
.options plotwinsize=0
.END
