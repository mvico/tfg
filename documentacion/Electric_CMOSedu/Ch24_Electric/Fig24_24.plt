[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524291,0,"V(vout)"} {524290,0,"V(vp)"}
      X: ('n',0,0,3e-008,3e-007)
      Y[0]: ('m',0,0.48,0.04,0.92)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,0.48,0.04,0.92)
      Log: 0 0 0
      GridStyle: 1
   }
}
