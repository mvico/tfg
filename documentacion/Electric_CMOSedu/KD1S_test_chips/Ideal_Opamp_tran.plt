[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(vout)"} {524291,0,"V(vin)"}
      X: ('n',0,0,4e-009,4e-008)
      Y[0]: (' ',2,2.49,0.01,2.6)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,2.49,0.01,2.6)
      Log: 0 0 0
      GridStyle: 1
   }
}
