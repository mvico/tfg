[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(vin)"} {524292,0,"(V(b[0])+V(b[1])+V(b[2])+V(b[3])+V(b[4])+V(b[5])+V(b[6])+V(b[7]))/8"}
      X: ('n',0,0,4e-008,4e-007)
      Y[0]: (' ',1,-0.6,0.6,6)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,-0.6,0.6,6)
      Log: 0 0 0
      GridStyle: 1
   }
}
