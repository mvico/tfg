[Transient Analysis]
{
   Npanes: 1
   {
      traces: 4 {524290,0,"V(vbias4)"} {524291,0,"V(vbias3)"} {524292,0,"V(vbias2)"} {524293,0,"V(vbias1)"}
      X: (' ',1,0,0.1,1)
      Y[0]: (' ',1,0.9,0.3,3.9)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,0.9,0.3,3.9)
      Log: 0 0 0
      GridStyle: 1
   }
}
