[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524291,0,"V(vop)-V(vom)"} {524290,0,"V(vip)-V(vim)"}
      X: ('n',0,0,2e-009,2e-008)
      Y[0]: (' ',1,-1.5,0.3,1.5)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,-1.5,0.3,1.5)
      Log: 0 0 0
      GridStyle: 1
   }
}
