[Transient Analysis]
{
   Npanes: 4
   Active Pane: 3
   {
      traces: 1 {524290,0,"V(clock)"}
      X: ('n',0,0,5e-009,5e-008)
      Y[0]: (' ',1,0,0.5,5)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,0,0.5,5)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {524291,0,"V(dclock)"}
      X: ('n',0,0,5e-009,5e-008)
      Y[0]: (' ',1,0,0.5,5)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,0,0.5,5)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {524292,0,"V(up)"}
      X: ('n',0,0,5e-009,5e-008)
      Y[0]: (' ',1,-0.5,0.5,5.5)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,-0.5,0.5,5.5)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {524293,0,"V(down)"}
      X: ('n',0,0,5e-009,5e-008)
      Y[0]: (' ',1,-0.5,0.5,5.5)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,-0.5,0.5,5.5)
      Log: 0 0 0
      GridStyle: 1
   }
}
