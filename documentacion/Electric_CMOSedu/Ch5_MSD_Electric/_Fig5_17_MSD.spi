*** SPICE deck for cell _Fig5_17_MSD{sch} from library Ch5_MSD
*** Created on Thu Apr 24, 2008 01:33:30
*** Last revised on Sat Oct 09, 2010 14:13:36
*** Written on Sun Nov 21, 2010 18:52:17 by Electric VLSI Design System, 
*version 9.00-q
*** Layout tech: mocmos, foundry MOSIS
*** UC SPICE *** , MIN_RESIST 4.0, MIN_CAPAC 0.1FF

*** SUBCIRCUIT Ideal_Switch FROM CELL Ideal_Switch{sch}
.SUBCKT Ideal_Switch contrlm contrlp in out

* Spice Code nodes in cell cell 'Ideal_Switch{sch}'
Sswitch  in out  contrlp contrlm  switmod
.model switmod sw
.ENDS Ideal_Switch

*** SUBCIRCUIT Ideal_DACbit FROM CELL Ideal_DACbit{sch}
.SUBCKT Ideal_DACbit Bitin Bitout Vone Vtrip
** GLOBAL gnd
Xideal_sw@0 Bitin Vtrip Bitout gnd Ideal_Switch
Xideal_sw@1 Vtrip Bitin Vone Bitout Ideal_Switch
.ENDS Ideal_DACbit

*** SUBCIRCUIT Ideal_8Bit_DAC FROM CELL Ideal_8Bit_DAC{sch}
.SUBCKT Ideal_8Bit_DAC B0 B1 B2 B3 B4 B5 B6 B7 VREFM VREFP Vout vdd
** GLOBAL gnd
Rres@0 vdd Vtrip 100000k
Rres@1 Vtrip gnd 100000k
Rres@2 VREFP VREFM 100000k
XIdeal_DA@1 B7 B7L Vone Vtrip Ideal_DACbit
XIdeal_DA@2 B6 B6L Vone Vtrip Ideal_DACbit
XIdeal_DA@3 B2 B2L Vone Vtrip Ideal_DACbit
XIdeal_DA@5 B5 B5L Vone Vtrip Ideal_DACbit
XIdeal_DA@6 B1 B1L Vone Vtrip Ideal_DACbit
XIdeal_DA@8 B4 B4L Vone Vtrip Ideal_DACbit
XIdeal_DA@9 B0 B0L Vone Vtrip Ideal_DACbit
XIdeal_DA@10 B3 B3L Vone Vtrip Ideal_DACbit

* Spice Code nodes in cell cell 'Ideal_8Bit_DAC{sch}'
Vone Vone 0 DC 1
B1 Vout 0 
+V=((v(vrefp)-v(vrefm))/256)*(v(B7L)*128+v(B6L)*64+v(B5L)*32+v(B4L)*16+v(B3L)*8+v(B2L)*4+v(B1L)*2+v(B0L))+v(vrefm)
.ENDS Ideal_8Bit_DAC

*** SUBCIRCUIT Ideal_Differencer_5 FROM CELL Ideal_Differencer_5{sch}
.SUBCKT Ideal_Differencer_5 im ip om op

* Spice Code nodes in cell cell 'Ideal_Differencer_5{sch}'
E  op  om  ip  im  2.1
.ENDS Ideal_Differencer_5

*** SUBCIRCUIT ADCbit_w_gain_error FROM CELL ADCbit_w_gain_error{sch}
.SUBCKT ADCbit_w_gain_error Bitout VCM Vin Vout Vtrip vdd
** GLOBAL gnd
XIdeal_Di@0 VCM Vin gnd Vinh Ideal_Differencer_5
XIdeal_Di@1 gnd Vin gnd Vinl Ideal_Differencer_5
XIdeal_sw@0 VCM Vin vdd Bitout Ideal_Switch
XIdeal_sw@1 Vin VCM Bitout gnd Ideal_Switch
XIdeal_sw@4 Vtrip Bitout Vinh Vout Ideal_Switch
XIdeal_sw@5 Bitout Vtrip Vout Vinl Ideal_Switch
.ENDS ADCbit_w_gain_error

*** SUBCIRCUIT Ideal_Opamp FROM CELL Ideal_Opamp{sch}
.SUBCKT Ideal_Opamp Vm Vo Vp
** GLOBAL gnd
Rres@0 Vp Vm 1410.065meg
Rres@1 Vo gnd 1

* Spice Code nodes in cell cell 'Ideal_Opamp{sch}'
G1 Vo 0  Vp  Vm 100MEG
.ENDS Ideal_Opamp

*** SUBCIRCUIT Ideal_Sample_Hold FROM CELL Ideal_Sample&Hold{sch}
.SUBCKT Ideal_Sample_Hold Vclk Vin Voutsh vdd
** GLOBAL gnd
Ccap@0 gnd Vins 0.1n
Ccap@1 gnd net@32 0.1f
Rres@0 vdd Vtrip 100000k
Rres@1 Vtrip gnd 100000k
XIdeal_sw@0 Vclk Vtrip Vinb Vins Ideal_Switch
XIdeal_sw@1 Vtrip Vclk Vins net@32 Ideal_Switch
Xideal_op@0 Voutsh Voutsh net@32 Ideal_Opamp
Xideal_op@1 Vinb Vinb Vin Ideal_Opamp
.ENDS Ideal_Sample_Hold

*** SUBCIRCUIT Ideal_8_bit_ADC_w_gain_error FROM CELL 
*Ideal_8_bit_ADC_w_gain_error{sch}
.SUBCKT Ideal_8_bit_ADC_w_gain_error B0 B1 B2 B3 B4 B5 B6 B7 VREFM VREFP Vin 
+clock vdd
** GLOBAL gnd
Rres@0 vdd Vtrip 100000k
Rres@1 Vtrip gnd 100000k
XADCbit_w@0 B7 VCM PIPIN net@77 Vtrip vdd ADCbit_w_gain_error
XADCbit_w@1 B6 VCM net@77 net@122 Vtrip vdd ADCbit_w_gain_error
XADCbit_w@2 B5 VCM net@122 net@124 Vtrip vdd ADCbit_w_gain_error
XADCbit_w@4 B4 VCM net@124 net@18 Vtrip vdd ADCbit_w_gain_error
XADCbit_w@6 B0 VCM net@135 ADCbit_w@6_Vout Vtrip vdd ADCbit_w_gain_error
XADCbit_w@7 B1 VCM net@141 net@135 Vtrip vdd ADCbit_w_gain_error
XADCbit_w@8 B2 VCM net@147 net@141 Vtrip vdd ADCbit_w_gain_error
XADCbit_w@9 B3 VCM net@18 net@147 Vtrip vdd ADCbit_w_gain_error
Xideal_Sa@0 clock Vin VOUTSH vdd Ideal_Sample_Hold

* Spice Code nodes in cell cell 'Ideal_8_bit_ADC_w_gain_error{sch}'
B1 VCM 0 V=(V(VREFP)-V(VREFM))/2
BPIP PIPIN 0 V=V(VOUTSH)-V(VREFM)+((V(VREFP)-V(VREFM))/512)
.ENDS Ideal_8_bit_ADC_w_gain_error

.global gnd vdd

*** TOP LEVEL CELL: _Fig5_17_MSD{sch}
XIdeal_8B@1 B0 B1 B2 B3 B4 B5 B6 B7 gnd vdd Vout vdd Ideal_8Bit_DAC
XIdeal_8_@0 B0 B1 B2 B3 B4 B5 B6 B7 gnd vdd Vin clock vdd 
+Ideal_8_bit_ADC_w_gain_error

* Spice Code nodes in cell cell '_Fig5_17_MSD{sch}'
Vin Vin 0 SINE(0.5 0.5 7MEG)
Vclock clock 0 PULSE(0 1 0 0 0 4.9n 10n)
VDD VDD 0 DC 1
.tran 0 2000n uic
.options plotwinsize=0
.END
