[Transient Analysis]
{
   Npanes: 1
   {
      traces: 4 {268959746,0,"V(vinsp)"} {268959747,0,"V(vinsm)"} {268959748,0,"V(voutp)"} {268959749,0,"V(voutm)"}
      X: ('n',0,0,5e-008,5e-007)
      Y[0]: ('m',0,-0.5,0.1,0.8)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.5,0.1,0.8)
      Log: 0 0 0
      GridStyle: 1
   }
}
