[Transient Analysis]
{
   Npanes: 1
   {
      traces: 3 {524290,0,"V(vin)"} {524291,0,"V(voutf)"} {524292,0,"V(vout)"}
      X: ('n',0,0,1e-008,1.1e-007)
      Y[0]: ('m',0,0.64,0.02,0.9)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,0.64,0.02,0.9)
      Log: 0 0 0
      GridStyle: 1
   }
}
