[DC transfer characteristic]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 1 {34603011,0,"Id(Mm1)"}
      X: (' ',1,1,0.4,5)
      Y[0]: ('n',0,3e-008,3e-008,3.9e-007)
      Y[1]: ('n',0,1e+308,3e-009,-1e+308)
      Amps: ('n',0,0,0,3e-008,3e-008,3.9e-007)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {524290,0,"d(Id(Mm1))"}
      X: (' ',1,1,0.4,5)
      Y[0]: ('n',0,6.3e-008,3e-009,9.3e-008)
      Y[1]: ('n',0,1e+308,3e-009,-1e+308)
      Units: "ohm-1" ('n',0,0,0,6.3e-008,3e-009,9.3e-008)
      Log: 0 0 0
      GridStyle: 1
   }
}
