[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"1/d(Id(Mm1))"}
      X: (' ',1,0,0.1,1)
      Y[0]: ('K',0,0,20000,180000)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "ohm" ('K',0,0,0,0,20000,180000)
      Log: 0 0 0
      GridStyle: 1
   }
}
