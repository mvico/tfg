[DC transfer characteristic]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 1 {34603011,0,"Id(Mm1)"}
      X: (' ',1,0,0.1,1)
      Y[0]: ('�',0,0,4e-006,3.6e-005)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Amps: ('�',0,0,0,0,4e-006,3.6e-005)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {524290,0,"d(Id(Mm1))"}
      X: (' ',1,0,0.1,1)
      Y[0]: ('�',0,0,8e-006,8.8e-005)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "ohm-1" ('�',0,0,0,0,8e-006,8.8e-005)
      Log: 0 0 0
      GridStyle: 1
   }
}
