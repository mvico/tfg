[Transient Analysis]
{
   Npanes: 1
   {
      traces: 3 {524290,0,"V(vbuck)"} {524291,0,"V(vbucki)"} {524292,0,"V(qbar)"}
      X: ('n',0,0,3e-008,3.6e-007)
      Y[0]: (' ',1,-0.1,0.1,1.1)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,-0.1,0.1,1.1)
      Log: 0 0 0
      GridStyle: 1
   }
}
