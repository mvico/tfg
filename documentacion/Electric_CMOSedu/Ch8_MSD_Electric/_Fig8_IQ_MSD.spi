*** SPICE deck for cell _Fig8_IQ_MSD{sch} from library Ch8_MSD
*** Created on Tue Jun 24, 2008 17:32:28
*** Last revised on Mon Dec 13, 2010 11:46:12
*** Written on Mon Dec 13, 2010 11:46:29 by Electric VLSI Design System, 
*version 9.00
*** Layout tech: mocmos, foundry MOSIS
*** UC SPICE *** , MIN_RESIST 4.0, MIN_CAPAC 0.1FF

*** SUBCIRCUIT Dep_source_1 FROM CELL Dep_source_1{sch}
.SUBCKT Dep_source_1 VAI VAQ VI VIQ_in VQ
** GLOBAL gnd
Rres@0 VAQ VAI 100000k
Rres@1 VQ VI 100000k
Rres@2 VIQ_in gnd 100000k

* Spice Code nodes in cell cell 'Dep_source_1{sch}'
B1 VIQ_in N003 V=V(VAI)*V(VI)
B2 N003 0 V=V(VAQ)*V(VQ)
.ENDS Dep_source_1

*** SUBCIRCUIT Ideal_Differencer_G_1 FROM CELL Ideal_Differencer_G=1{sch}
.SUBCKT Ideal_Differencer_G_1 im ip om op

* Spice Code nodes in cell cell 'Ideal_Differencer_G=1{sch}'
E  op  om  ip  im  1
.ENDS Ideal_Differencer_G_1

*** SUBCIRCUIT Ideal_Differencer_G_0_5 FROM CELL Ideal_Differencer_G=0.5{sch}
.SUBCKT Ideal_Differencer_G_0_5 im ip om op

* Spice Code nodes in cell cell 'Ideal_Differencer_G=0.5{sch}'
E  op  om  ip  im  0.5
.ENDS Ideal_Differencer_G_0_5

*** SUBCIRCUIT Ideal_Switch FROM CELL Ideal_Switch{sch}
.SUBCKT Ideal_Switch contrlm contrlp in out

* Spice Code nodes in cell cell 'Ideal_Switch{sch}'
Sswitch  in out  contrlp contrlm  switmod
.model switmod sw
.ENDS Ideal_Switch

*** SUBCIRCUIT Ideal_Op_amp FROM CELL Ideal_Op_amp{sch}
.SUBCKT Ideal_Op_amp Vm Vo Vp
** GLOBAL gnd
Rres@0 Vp Vm 1410.065meg
Rres@1 Vo gnd 1

* Spice Code nodes in cell cell 'Ideal_Op_amp{sch}'
G1 0 Vo  Vp  Vm 100MEG
.ENDS Ideal_Op_amp

*** SUBCIRCUIT Ideal_Sample_Hold FROM CELL Ideal_Sample&Hold{sch}
.SUBCKT Ideal_Sample_Hold Vclk Vin Voutsh vdd
** GLOBAL gnd
Ccap@0 Vins gnd 0.1n
Ccap@1 net@60 gnd 0.1f
Rres@0 vdd Vtrip 100000k
Rres@1 Vtrip gnd 100000k
XIdeal_sw@0 Vclk Vtrip Vinb Vins Ideal_Switch
XIdeal_sw@1 Vtrip Vclk Vins net@60 Ideal_Switch
Xideal_op@0 Voutsh Voutsh net@60 Ideal_Op_amp
Xideal_op@1 Vinb Vinb Vin Ideal_Op_amp
.ENDS Ideal_Sample_Hold

*** SUBCIRCUIT Ideal_DFF FROM CELL Ideal_DFF{sch}
.SUBCKT Ideal_DFF D Q Qi clk vdd
** GLOBAL gnd
XIdeal_Di@0 gnd vdd gnd Vtrip Ideal_Differencer_G_0_5
XIdeal_Sa@0 clk D Vshp vdd Ideal_Sample_Hold
XIdeal_Sw@0 Vtrip Vshp vdd Q Ideal_Switch
XIdeal_Sw@1 Vshp Vtrip Q gnd Ideal_Switch
XIdeal_Sw@2 Vshp Vtrip vdd Qi Ideal_Switch
XIdeal_Sw@3 Vtrip Vshp Qi gnd Ideal_Switch
.ENDS Ideal_DFF

*** SUBCIRCUIT Ideal_Differencer_G_2 FROM CELL Ideal_Differencer_G=2{sch}
.SUBCKT Ideal_Differencer_G_2 im ip om op

* Spice Code nodes in cell cell 'Ideal_Differencer_G=2{sch}'
E  op  om  ip  im  2
.ENDS Ideal_Differencer_G_2

*** SUBCIRCUIT Ideal_differential_op_amp FROM CELL 
*Ideal_differential_op_amp{sch}
.SUBCKT Ideal_differential_op_amp Outm Outp VCM Vinm Vinp
Rres@0 Vinp Vinm 1410.065meg
Rres@1 Outp Vcom 1
Rres@2 Vcom Outm 1
Rres@3 VCM Vcom 1410.065meg

* Spice Code nodes in cell cell 'Ideal_differential_op_amp{sch}'
G1 Vcom Outp Vinp Vinm 100MEG
G2 Outm Vcom Vinp Vinm 100MEG
G3 0 Vcom VCM Vcom 100MEG
.ENDS Ideal_differential_op_amp

*** SUBCIRCUIT Ideal_differential_clocked_comparator FROM CELL 
*Ideal_differential_clocked_comparator{sch}
.SUBCKT Ideal_differential_clocked_comparator Clock Vinm Vinp Voutm Voutp vdd
** GLOBAL gnd
XIdeal_Sa@0 Clock Vinp Vshp vdd Ideal_Sample_Hold
XIdeal_Sa@1 Clock Vinm Vshm vdd Ideal_Sample_Hold
XIdeal_Sw@0 Vshm Vshp vdd Voutp Ideal_Switch
XIdeal_Sw@1 Vshp Vshm Voutp gnd Ideal_Switch
XIdeal_Sw@2 Vshp Vshm vdd Voutm Ideal_Switch
XIdeal_Sw@3 Vshm Vshp Voutm gnd Ideal_Switch
.ENDS Ideal_differential_clocked_comparator

*** SUBCIRCUIT Ideal_inverter FROM CELL Ideal_inverter{sch}
.SUBCKT Ideal_inverter In Out vdd
** GLOBAL gnd
XIdeal_Di@1 gnd vdd gnd net@8 Ideal_Differencer_G_0_5
XIdeal_Sw@0 In net@8 vdd Out Ideal_Switch
XIdeal_Sw@1 net@8 In Out gnd Ideal_Switch
.ENDS Ideal_inverter

*** SUBCIRCUIT Selectorbit FROM CELL Selectorbit{sch}
.SUBCKT Selectorbit BitA BitB O select vdd
** GLOBAL gnd
XIdeal_Di@0 gnd vdd gnd Vtrip Ideal_Differencer_G_0_5
XIdeal_Sw@0 Vtrip select O BitB Ideal_Switch
XIdeal_Sw@1 select Vtrip O BitA Ideal_Switch
.ENDS Selectorbit

*** SUBCIRCUIT switch_1 FROM CELL switch_1{sch}
.SUBCKT switch_1 P1 P2 clock vdd
** GLOBAL gnd
Rres@0 vdd Vtrip 100000k
Rres@1 Vtrip gnd 100000k
XIdeal_Sw@0 Vtrip clock P1 P2 Ideal_Switch
.ENDS switch_1

*** SUBCIRCUIT switches_2 FROM CELL switches_2{sch}
.SUBCKT switches_2 P1 P2 P3 clk1 clk2 vdd
** GLOBAL gnd
RR1 Vtrip gnd 100000k
RR2 vdd Vtrip 100000k
RR3 R3_b gnd 100000k
XIdeal_Sw@0 Vtrip clk1 P1 P2 Ideal_Switch
XIdeal_Sw@1 Vtrip clk2 P2 P3 Ideal_Switch
.ENDS switches_2

.global gnd

*** TOP LEVEL CELL: _Fig8_IQ_MSD{sch}
CC1 gnd net@242 1n
CC2 gnd net@258 1n
CCF1 net@64 net@146 1p
CCF1m net@0 net@107 1p
CCF2 net@60 net@76 1p
CCF3 net@2 net@1 1p
CCF4 net@21 net@183 1p
CCF5 net@88 net@85 1p
CCF6 net@83 net@74 1p
CCI1 net@156 net@151 2p
CCI1m net@25 net@16 {1p*G1}
CCI1p net@158 net@157 {1p*G1}
CCI2 net@39 net@31 2p
CCI3 net@142 net@141 {1p*G1}
CCI4 net@112 net@102 {1p*G1}
CCI5 net@140 net@138 2p
CCI6 net@127 net@119 2p
CCI7 net@4 net@7 {1p*G2}
CCI8 net@180 net@179 {1p*G2}
CCI9 net@6 net@5 2p
CCI10 net@177 net@181 2p
CCI11 net@90 net@93 {1p*G2}
CCI12 net@71 net@70 {1p*G2}
CCI13 net@92 net@91 2p
CCI14 net@68 net@72 2p
CCP1p net@11 net@163 1p
RR1 net@242 Inphase 1000
RR2 net@258 Quad 1000
XDep_sour@0 VAI VAQ VI VIQ_in VQ Dep_source_1
XE4 gnd B1Q net@264 Quad Ideal_Differencer_G_1
XE5 gnd B0Q net@273 net@264 Ideal_Differencer_G_0_5
XIdeal_DF@0 net@204 net@202 net@204 phi1 vdd Ideal_DFF
XIdeal_Di@7 gnd B1I net@236 Inphase Ideal_Differencer_G_1
XIdeal_Di@8 gnd B0I net@275 net@236 Ideal_Differencer_G_0_5
XIdeal_Di@13 gnd VIQ_in VCM Vinsp Ideal_Differencer_G_2
XIdeal_Di@14 gnd VIQ_in Vinsm VCM Ideal_Differencer_G_2
XIdeal_di@1 net@83 net@88 VCM net@85 net@74 Ideal_differential_op_amp
XIdeal_di@2 phi2 net@83 net@88 Voutp2 Voutm2 vdd 
+Ideal_differential_clocked_comparator
XIdeal_di@3 net@0 net@11 VCM net@163 net@107 Ideal_differential_op_amp
XIdeal_di@4 net@21 net@2 VCM net@1 net@183 Ideal_differential_op_amp
XIdeal_di@5 phi1 net@21 net@2 Voutp1 Voutm1 vdd 
+Ideal_differential_clocked_comparator
XIdeal_di@6 net@60 net@64 VCM net@146 net@76 Ideal_differential_op_amp
XIdeal_in@0 net@196 B1I vdd Ideal_inverter
XIdeal_in@1 net@224 B1Q vdd Ideal_inverter
XIdeal_in@2 phi1 phi1b vdd Ideal_inverter
XSelector@0 net@194 gnd net@196 phi1 vdd Selectorbit
XSelector@1 vdd gnd B0I phi1 vdd Selectorbit
XSelector@2 Voutp Voutm net@194 net@202 vdd Selectorbit
XSelector@3 net@194 gnd net@224 phi1b vdd Selectorbit
XSelector@4 vdd gnd B0Q phi1b vdd Selectorbit
VV1 net@275 gnd DC -0.5V
VV2 net@273 gnd DC -0.5V
VV3 net@258 Quad_f DC 0.5V
VV4 net@242 Inphase_f DC 0.5V
Xswitch_1@0 Voutp1 Voutp phi1 vdd switch_1
Xswitch_1@1 Voutm1 Voutm phi1 vdd switch_1
Xswitch_1@2 Voutp2 Voutp phi2 vdd switch_1
Xswitch_1@3 Voutm2 Voutm phi2 vdd switch_1
Xswitches@0 VCM net@16 net@107 phi2 phi1 vdd switches_2
Xswitches@1 Vinsm net@25 Voutm1 phi2 phi1 vdd switches_2
Xswitches@2 net@0 net@180 Voutm1 phi1 phi2 vdd switches_2
Xswitches@3 VCM net@181 net@183 phi1 phi2 vdd switches_2
Xswitches@4 VCM net@6 net@1 phi1 phi2 vdd switches_2
Xswitches@5 net@11 net@7 Voutp1 phi1 phi2 vdd switches_2
Xswitches@6 VCM net@4 net@1 phi1 phi2 vdd switches_2
Xswitches@7 net@21 net@5 VCM phi1 phi2 vdd switches_2
Xswitches@8 VCM net@102 net@76 phi1 phi2 vdd switches_2
Xswitches@9 Vinsm net@112 Voutm2 phi1 phi2 vdd switches_2
Xswitches@10 net@60 net@71 Voutm2 phi2 phi1 vdd switches_2
Xswitches@11 VCM net@72 net@74 phi2 phi1 vdd switches_2
Xswitches@12 VCM net@31 net@107 phi2 phi1 vdd switches_2
Xswitches@13 VCM net@92 net@85 phi2 phi1 vdd switches_2
Xswitches@14 net@64 net@93 Voutp2 phi2 phi1 vdd switches_2
Xswitches@15 VCM net@90 net@85 phi2 phi1 vdd switches_2
Xswitches@16 net@83 net@91 VCM phi2 phi1 vdd switches_2
Xswitches@17 VCM net@119 net@76 phi1 phi2 vdd switches_2
Xswitches@18 net@64 net@127 VCM phi1 phi2 vdd switches_2
Xswitches@19 net@60 net@138 VCM phi1 phi2 vdd switches_2
Xswitches@20 VCM net@140 net@146 phi1 phi2 vdd switches_2
Xswitches@21 Vinsp net@141 Voutp2 phi1 phi2 vdd switches_2
Xswitches@22 VCM net@142 net@146 phi1 phi2 vdd switches_2
Xswitches@23 net@11 net@39 VCM phi2 phi1 vdd switches_2
Xswitches@24 net@88 net@68 VCM phi2 phi1 vdd switches_2
Xswitches@25 VCM net@70 net@74 phi2 phi1 vdd switches_2
Xswitches@26 net@0 net@151 VCM phi2 phi1 vdd switches_2
Xswitches@27 VCM net@156 net@163 phi2 phi1 vdd switches_2
Xswitches@28 Vinsp net@157 Voutp1 phi2 phi1 vdd switches_2
Xswitches@29 VCM net@158 net@163 phi2 phi1 vdd switches_2
Xswitches@30 net@2 net@177 VCM phi1 phi2 vdd switches_2
Xswitches@31 VCM net@179 net@183 phi1 phi2 vdd switches_2

* Spice Code nodes in cell cell '_Fig8_IQ_MSD{sch}'
VCM VCM 0 DC 0.5
VDD VDD 0 DC 1
Vphi1 phi1 0 PULSE(0 1 10n 200p 200p 9n 20n)
Vphi2 phi2 0 PULSE(0 1 20n 200p 200p 9n 20n)
VAI VAI 0 SINE(0 0.2 60k)
VAQ VAQ 0 SINE(0 0.1 50k)
VI VI 0 SINE(0 1 25MEG 10n)
VQ VQ 0 SINE(0 1 25MEG)
.tran 0 52u 2u uic
.options plotwinsize=0
.options cshunt=10f
.param G1=0.4 G2=0.4
.END
