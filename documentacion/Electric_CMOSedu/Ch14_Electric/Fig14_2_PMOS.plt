[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"log10(Id(Mm1))"}
      X: (' ',1,-0.1,0.1,1)
      Y[0]: (' ',1,-12.5,0.5,-7)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "" (' ',0,0,0,-12.5,0.5,-7)
      Log: 0 0 0
      GridStyle: 1
   }
}
