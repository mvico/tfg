[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(vout)"} {524291,0,"V(vin)"}
      X: ('n',0,0,1e-007,1e-006)
      Y[0]: (' ',2,2.48,0.02,2.66)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,2.48,0.02,2.66)
      Log: 0 0 0
      GridStyle: 1
   }
}
