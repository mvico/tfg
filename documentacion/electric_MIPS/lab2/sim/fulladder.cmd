| Simulate a 1-bit full-adder
| Written by Pallav Gupta (pallav.gupta@villanova.edu)

vector in a b c

set in 000
s 2
assert s 0
assert cout 0

set in 001
s 2
assert s 1
assert cout 0

