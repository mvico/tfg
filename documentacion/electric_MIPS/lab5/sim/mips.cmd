| Perform chip-level simulation on the 8-bit microprocessor.
| Test the various instructions by running a small assembly language program.
| Written by Pallav Gupta (pallav.gupta@villanova.edu)

|*******************************************************************
| main:  # Assembly code       Effect                    Machine code
|        lb $2, 68($0)         # initialize $2 = 5       80020044
|        lb $7, 64($0)         # initialize $7 = 3       80070040
|        lb $3, 69($7)         # initialize $3 = 12      80e30045
|        or $4, $7, $2         # $4 <= (3 or 5 = 7)      00e22025
|        and $5, $3, $4        # $5 <= (12 and 7 = 4)    00642824
|        add $5, $5, $4        # $5 <= (4 + 7 = 11)      00a42820
|        beq $5, $7, end       # shouldn't be taken      10a70008
|        slt $6, $3, $4        # $6 <= (12 < 7 = 0)      0064302a
|        beq $6, $0, around    # should be taken         10c00001
|        lb $5, 0($0)          # shouldn't happen        80050000
| around:
|        slt $6, $7, $2        # $6 <= (3 < 5 = 1)       00e2302a
|        add $7, $6, $5        # $7 <= (1 + 11 = 12)     00c53820
|        sub $7, $7, $2        # $7 <= (12 - 5 = 7)      00e23822
|        j end                 # should be taken         0800000f
|        lb $7, 0($0)          # shouldn't happen        80070000
| end:
|        sb $7, 0($2)          # write adr 5 <= 7        a0470000
|******************************************************************* 


| Define vectors for convenience
vector memdata memdata[7] memdata[6] memdata[5] memdata[4] memdata[3] memdata[2] memdata[1] memdata[0]
vector writedata writedata[7] writedata[6] writedata[5] writedata[4] writedata[3] writedata[2] writedata[1] writedata[0]
vector adr adr[7] adr[6] adr[5] adr[4] adr[3] adr[2] adr[1] adr[0]

| Setup a 2-phase nonoverlapping clock with a time of 10 ns for each phase (40 ns cycle time -> 25 MHz)
clock ph1 1 0 0 0
clock ph2 0 0 1 0
stepsize 10

| Do hart reset, sets PC <= 00000000
h reset
c
c
l reset

|------------------------------------------------------
| lb $2, 68($0)       # intiialize $2 = 5      80020044
|------------------------------------------------------

| state 0
set memdata 10000000
c

| state 1
assert adr 00000000
assert memread 1
assert memwrite 0

set memdata 00000010
c

| state 2
assert adr 00000001
assert memread 1
assert memwrite	0

set memdata 00000000
c

| state 3
assert adr 00000010
assert memread 1
assert memwrite 0

set memdata 01000100
c

| state 4
assert adr 00000011
assert memread 1
assert memwrite 0
c

| state 5
assert memread 0
assert memwrite 0
c

| state 6
assert memread 0
assert memwrite 0

set memdata 00000101
c

| state 7
assert adr 01000100
assert memread 1
assert memwrite 0
c

assert memread 0
assert memwrite 0

|------------------------------------------------------
| lb $7, 64($0)     # initialize $7 = 3       80070040
|------------------------------------------------------

| state 0
set memdata 10000000
c

| state 1
assert adr 00000100
assert memread 1
assert memwrite 0

set memdata 00000111
c

| state 2
assert adr 00000101
assert memread 1
assert memwrite 0

set memdata 00000000
c

| state 3
assert adr 00000110
assert memread 1
assert memwrite	0

set memdata 01000000
c

| state 4
assert adr 00000111
assert memread 1
assert memwrite	0
c

| state 5
assert memread 0
assert memwrite 0
c

| state 6
assert memread 0
assert memwrite 0

set memdata 00000011
c

| state 7
assert adr 01000000
assert memread 1
assert memwrite 0
c

assert memread 0
assert memwrite 0

|------------------------------------------------------
| lb $3, 69($7)     # initialize $3 = 12      80e30045
|------------------------------------------------------

| Add this instruction

|------------------------------------------------------
| or $4, $7, $2     # $4 <= (3 or 5 = 7)      00e22025
|------------------------------------------------------

| Add this instruction

|------------------------------------------------------
| and $5, $3, $4   # $5 <= (12 and 7 = 4)    00642824
|------------------------------------------------------

| state 0
set memdata 00000000
c

| state 1
assert adr 00010000
assert memread 1
assert memwrite	0

set memdata 01100100
c

| state 2
assert adr 00010001
assert memread 1
assert memwrite	0

set memdata 00101000
c

| state 3
assert adr 00010010
assert memread 1
assert memwrite	0

set memdata 00100100
c

| state 4
assert adr 00010011
assert memread 1
assert memwrite	0
c

| state 9
assert memread 0
assert memwrite	0
c

| state 10
assert memread 0
assert memwrite 0
c

assert memread 0
assert memwrite 0

|------------------------------------------------------
| add $5, $5, $4   # $5 <= (4 + 7 = 11)      00a42820
|------------------------------------------------------

| state 0
set memdata 00000000
c

| state 1
assert adr 00010100
assert memread 1
assert memwrite	0

set memdata 10100100
c

| state 2
assert adr 00010101
assert memread 1
assert memwrite	0

set memdata 00101000
c

| state 3
assert adr 00010110
assert memread 1
assert memwrite	0

set memdata 00100000
c

| state 4
assert adr 00010111
assert memread 1
assert memwrite	0
c

| state 9
assert memread 0
assert memwrite	0
c

| state 10
assert memread 0
assert memwrite 0
c

assert memread 0
assert memwrite 0

|------------------------------------------------------
| beq $5, $7, end   # shouldn't be taken      10a70008
|------------------------------------------------------

| state 0
set memdata 00010000
c

| state 1
assert adr  00011000
assert memread 1
assert memwrite	0

set memdata 10100111
c

| state 2
assert adr 00011001
assert memread 1
assert memwrite	0

set memdata 00000000
c

| state 3
assert adr 00011010
assert memread 1
assert memwrite	0

set memdata 00001000
c

| state 4
assert adr 00011011
assert memread 1
assert memwrite 0
c

| state 11
assert memread 0
assert memwrite 0
c

assert memread 	0
assert memwrite 0

|------------------------------------------------------
| slt $6, $3, $4    # $6 <= (12 < 7 = 0)       0064302a
|------------------------------------------------------

| state 0
set memdata 00000000
c

| state 1
assert adr 00011100
assert memread 1
assert memwrite	0

set memdata 01100100
c

| state 2
assert adr 00011101
assert memread 1
assert memwrite	0

set memdata 00110000
c

| state 3
assert adr 00011110
assert memread 1
assert memwrite	0

set memdata 00101010
c

| state 4
assert adr 00011111
assert memread 1
assert memwrite	0
c

| state 9
assert memread 0
assert memwrite	0
c

| state 10
assert memread 0
assert memwrite 0
c

assert memread 0
assert memwrite 0

|------------------------------------------------------
| beq $6, $0, around  # should be taken       10c00001
|------------------------------------------------------

| state 0
set memdata 00010000
c

| state 1
assert adr  00100000
assert memread 1
assert memwrite	0

set memdata 11000000
c

| state 2
assert adr 00100001
assert memread 1
assert memwrite	0

set memdata 00000000
c

| state 3
assert adr 00100010
assert memread 1
assert memwrite	0

set memdata 00000001
c

| state 4
assert adr 00100011
assert memread 1
assert memwrite 0
c

| state 11
assert memread 0
assert memwrite 0
c

assert memread 	0
assert memwrite 0

|------------------------------------------------------
| slt $6, $7, $2   # $6 <= (3 < 5 = 1)       00e2302a
|------------------------------------------------------

| state 0
set memdata 00000000
c

| state 1
assert adr 00101000
assert memread 1
assert memwrite	0

set memdata 11100010
c

| state 2
assert adr 00101001
assert memread 1
assert memwrite	0

set memdata 00110000
c

| state 3
assert adr 00101010
assert memread 1
assert memwrite	0

set memdata 00101010
c

| state 4
assert adr 00101011
assert memread 1
assert memwrite	0
c

| state 9
assert memread 0
assert memwrite	0
c

| state 10
assert memread 0
assert memwrite 0
c

assert memread 0
assert memwrite 0

|------------------------------------------------------
| add $7, $6, $5    # $7 <= (1 + 11 = 12)     00c53820
|------------------------------------------------------

| state 0
set memdata 00000000
c

| state 1
assert adr 00101100
assert memread 1
assert memwrite	0

set memdata 11000101
c

| state 2
assert adr 00101101
assert memread 1
assert memwrite	0

set memdata 00111000
c

| state 3
assert adr 00101110
assert memread 1
assert memwrite	0

set memdata 00100000
c

| state 4
assert adr 00101111
assert memread 1
assert memwrite	0
c

| state 9
assert memread 0
assert memwrite	0
c

| state 10
assert memread 0
assert memwrite 0
c

assert memread 0
assert memwrite 0

|------------------------------------------------------
| sub $7, $7, $2   # $7 <= (12 - 5 = 7)      00e23822
|------------------------------------------------------
| state 0
set memdata 00000000
c

| state 1
assert adr 00110000
assert memread 1
assert memwrite	0

set memdata 11100010
c

| state 2
assert adr 00110001
assert memread 1
assert memwrite	0

set memdata 00111000
c

| state 3
assert adr 00110010
assert memread 1
assert memwrite	0

set memdata 00100010
c

| state 4
assert adr 00110011
assert memread 1
assert memwrite	0
c

| state 9
assert memread 0
assert memwrite	0
c

| state 10
assert memread 0
assert memwrite 0
c

assert memread 0
assert memwrite 0

|------------------------------------------------------
| j end            # should be taken         0800000f
|------------------------------------------------------

| state 0
set memdata 00001000
c

| state 1
assert adr 00110100
assert memread 1
assert memwrite	0

set memdata 00000000
c

| state 2
assert adr 00110101
assert memread 1
assert memwrite	0

set memdata 00000000
c

| state 3
assert adr 00110110
assert memread 1
assert memwrite 0

set memdata 00001111
c

| state 4
assert adr 00110111
assert memread 1
assert memwrite	0
c

| state 12
assert memread 0
assert memwrite	0
c

assert memread 0
assert memwrite	0

|------------------------------------------------------
| sb $7, 0($2)     # write adr 5 <= 7        a0470000
|------------------------------------------------------

| state 0
set memdata		10100000
c

| state 1
assert adr 		00111100
assert memread 		1
assert memwrite 	0

set memdata		01000111
c

| state 2
assert adr 		00111101
assert memread 		1
assert memwrite 	0

set memdata             00000000
c

| state 3
assert adr 		00111110
assert memread 		1
assert memwrite 	0

set memdata             00000000
c

| state 4
assert adr 		00111111
assert memread 		1
assert memwrite 	0
c

| state 5
assert memread 		0
assert memwrite 	0
c

| state 8
assert memread 		0
assert memwrite 	0
c

assert memread 		0
assert memwrite 	1
assert writedata	00000111
assert adr		00000101

| Hooray, everything passes!
