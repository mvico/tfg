; Pad Frame arrangement file
; Pallav Gupta (pallav.gupta@villanova.edu)
;
; Taken from David Harris from HMC.
; Generate a padframe for a 40-pin MOSIS tinychip schematic

; specify the cell library with the pads
celllibrary iopads-ami05.jelib

; create a top-level cell containing the padframe and core
cell chip

; create the schematic view
views sch

; place this cell as the "core"
core mips8

; set the alignment of the pads (specifying input and output port names)
align pad_corner      dvdd-1 dvdd
align pad_in          dvdd-1 dvdd
align pad_out         dvdd-1 dvdd
align pad_analog	  dvdd-1 dvdd
align pad_dvdd        dvdd-1 dvdd
align pad_dgnd        dvdd-1 dvdd

;; replace the pad_in (inputs) and pad_out (outputs) statements with the pad
;; frame arrangement you want. keep at least two pad_dvdd and pad_dgnd pads
;; somewhere on the chip to supply power and ground to the pads and core

; place the top edge of pads, starting with upper-right
place pad_corner
place pad_out dout=memread export pad=memread
place pad_out dout=memwrite export pad=memwrite
place pad_in din=ph1 export pad=ph1
place pad_in din=ph2 export pad=ph2
place pad_in din=memdata[7] export pad=memdata7
place pad_in din=memdata[6] export pad=memdata6
place pad_in din=memdata[5] export pad=memdata5
place pad_in din=memdata[4] export pad=memdata4
place pad_in din=memdata[3] export pad=memdata3
place pad_in din=memdata[2] export pad=memdata2

; place the left edge of pads
rotate cc
place pad_corner
place pad_in din=memdata[1] export pad=memdata1
place pad_in din=memdata[0] export pad=memdata0
place pad_out dout=writedata[7] export pad=writedata7
place pad_out dout=writedata[6] export pad=writedata6
place pad_out dout=writedata[5] export pad=writedata5
place pad_out dout=writedata[4] export pad=writedata4
place pad_out dout=writedata[3] export pad=writedata3
place pad_out dout=writedata[2] export pad=writedata2
place pad_out dout=writedata[1] export pad=writedata1
place pad_out dout=writedata[0] export pad=writedata0

; place the bottom edge of pads
rotate cc
place pad_corner
place pad_out dout=adr[7] export pad=adr7
place pad_out dout=adr[6] export pad=adr6
place pad_out dout=adr[5] export pad=adr5
place pad_out dout=adr[4] export pad=adr4
place pad_out dout=adr[3] export pad=adr3
place pad_out dout=adr[2] export pad=adr2
place pad_out dout=adr[1] export pad=adr1
place pad_out dout=adr[0] export pad=adr0
place pad_dvdd 
place pad_dgnd

; place the right edge of pads
rotate cc
place pad_corner
place pad_dgnd
place pad_dvdd 
place pad_dgnd
place pad_dvdd 
place pad_dgnd
place pad_dvdd 
place pad_dgnd
place pad_in din=reset export pad=reset
place pad_dgnd export pad=GND
place pad_dvdd export pad=VDD
