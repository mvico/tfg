| Simulate a 2-input OR gate in IRSIM
| Written by Pallav Gupta (pallav.gupta@villanova.edu)

| 00
| a b
l a b
s 2
assert y 0

| 01
h a
s 2
assert y 1

| 11
h b
s 2
assert y 1

| 10
l a
s 2
assert y 1

l b
