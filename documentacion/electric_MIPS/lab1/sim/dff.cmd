| Simulate a D flip-flop in IRSIM
| Written by Pallav Gupta (pallav.gupta@villanova.edu)

stepsize 1

clock ph1    0 1 0 0
clock ph1b 1 0 1 1
clock ph2    0 0 0 1
clock ph2b 1 1 1 0

h d
c 
l d
c 
h d
c 
l d
c 2
h d
c 2
l d 
c 
h d
c 3
