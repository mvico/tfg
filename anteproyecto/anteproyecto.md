---
subtitle: "Anteproyecto del Trabajo Final de Grado para obtener el título de Ingeniero en Electrónica"
---

# Memoria Descriptiva

En el marco de la realización del trabajo final de grado, es que se propone el desarrollo del siguiente tema: "Diseño y verificación física de un amplificador operacional diferencial de dos etapas con compensación tipo Miller, basado en las reglas del proceso CMOS ONSEMI C5N de MOSIS ($0,5 \mu m$)".

En vista de las restricciones de operación que pueden presentar los amplificadores de una sola etapa, junto al hecho de alcanzar las especificaciones de trabajo requeridas en lazo abierto para el dispositivo, es que se plantea el desarrollo de un amplificador con dos etapas activas.

A su vez, a fines de lograr las especificaciones en lazo cerrado, se ve necesaria la implementación de una etapa de compensación pasiva entre ambas etapas de amplificación de donde surge el uso de la teoría de Miller.

De la comparación de las diferentes simulaciones realizadas para el amplificador con y sin la etapa de compensación tipo Miller, se verifica la ventaja de la implementación de la última.


\newpage


# Introducción

El presente informe sintetiza los lineamientos generales que darán inicio al trabajo final de grado para la carrera de Ingeniería en Electrónica de la Universidad Tecnológica Nacional, Facultad Regional Villa María. Serán también expresados los objetivos que intentarán alcanzarse en su desarrollo.

El proyecto propuesto a desarrollar consiste en el diseño de un amplificador operacional de dos etapas, con compensación Miller.

La realización del mismo será asumiendo las reglas de diseño del nodo de fabricación de $0,5 \mu m$ CMOS ONSEMI C5N de MOSIS. El desarrollo será llevado a cabo con el soporte de herramientas informáticas no libres, de pago y cerradas, así como también mediante el uso de herramientas de software libre, gratuito y abiertas para las diferentes etapas de dibujo, simulación, e implementación física, así como la verificación del cumplimiento de las reglas de diseño impuestas por el fabricante para el nodo elegido.


# Objetivos

## Objetivo general

- Diseñar un amplificador operacional de dos etapas con compensación Miller.

## Objetivos específicos

- Definir las condiciones de operación del amplificador.
- Comparar los resultados de las simulaciones realizadas en las diferentes etapas de diseño con las características de funcionamiento definidas para la operación del amplificador.
- Asegurar la factibilidad de su fabricación mediante la verificación del cumplimiento de todas las reglas de diseño para el nodo elegido.

# Diagrama del proceso de diseño del amplificador

\begin{center}
  \begin{tikzpicture}
    \node [startstop] (especificaciones) {Especificaciones de diseño};
    \node [block, below = of especificaciones] (simbolo) {Creación de símbolos};
    \node [block, below = of simbolo] (amp_design) {Diseño de etapas amplificadoras};
    \node [block, below = of amp_design] (miller_design) {Diseño de etapa Miller};
    \node [block, below = of miller_design] (simulacion) {Simulación};
    \node [block, right = of simulacion] (layout) {Layout};
    \node [block, above = of layout] (verificacion_fisica) {Verificación física};
    \node [block, above = of verificacion_fisica] (extraccion) {Extracción de parásitos};
    \node [block, above = of extraccion] (simulacion_post_layout) {Simulación post-layout};
    \node [block, above = of simulacion_post_layout] (tape_out) {Generación de fabricables};
    \node [anchor_block, left = of simulacion] (nodo_realimentacion) {};

    \draw [arrow] (especificaciones) -- (simbolo);
    \draw [arrow] (simbolo) -- (amp_design);
    \draw [arrow] (amp_design) -- (miller_design);
    \draw [arrow] (miller_design) -- (simulacion);
    \draw [arrow] (simulacion) -- (layout);
    \draw [arrow] (layout) -- (verificacion_fisica);
    \draw [arrow] (verificacion_fisica) -- (extraccion);
    \draw [arrow] (extraccion) -- (simulacion_post_layout);
    \draw [arrow] (simulacion_post_layout) -- (tape_out);

    \draw [line] (simulacion) -- (nodo_realimentacion.center);
    \draw [arrow] (nodo_realimentacion.center) |- (amp_design);
    \draw [arrow] (simulacion_post_layout) -- (amp_design);
  \end{tikzpicture}
\end{center}


# Descripción del trabajo a realizar

Partiendo de una serie de requisitos de operación determinados, se lleva adelante el diseño de un amplificador operacional de dos etapas con compensación pasiva tipo Miller.

El diseño del amplificador será realizado en diferentes etapas, siendo la metodología de trabajo elegida la jerárquica, yendo desde lo general hacia lo particular en el proyecto en su conjunto así como también en cada uno de los escalones de la jerarquía.

Se comienza el trabajo sobre el amplificador partiendo desde: la abstracción de la definición de sus características de operación, pasando por su diseño esquemático, creación de símbolos para su representación en jerarquías, implementación física, extracción de componentes parásitos, verificación física y simulaciones del comportamiento post-layout, etapas en las que avanzará su definición contemplando fenómenos y restricciones cada vez más reales desde el punto de vista del modelado físico del nodo de fabricación elegido. Con el avance de las etapas descriptas se ira aportando cada vez más detalle en la definición y condiciones de operación finales del amplificador.

Para el desarrollo del presente trabajo será preciso la utilización de varios tipos de software de diseño entre los que se encuentran: de simulación, para verificar el cumplimiento de los requisitos de operación en las distintas etapas de avance del proyecto; CAD, para dar soporte al dibujo de la disposición de los elementos activos que conformarán al amplificador y sus componentes de soporte; software específico para la extracción de los componentes parásitos en el diseño; de verificación física, para asegurar el cumplimiento de las condiciones de fabricación impuestas por el nodo y el fabricante; entre otros.

El proyecto finaliza con la generación y verificación de la integridad de los archivos de fabricación, según el cumplimiento de todas las reglas y restricciones impuestas por el "archivo de tecnología" para el nodo en cuestión.


\newpage


# Plan de avance

\begin{center}
  \begin{ganttchart}{1}{42}

    % 24 unidades / 6 meses = 6 unidades/mes
    % 6 unidades = 30 días
    % 1 unidad = 5 días ~= 1 semana

    \gantttitle{Distribución de las tareas del proyecto} {42}	\\
    \gantttitle{Sep} {6}
    \gantttitle{Oct} {6}
    \gantttitle{Nov} {6}
    \gantttitle{Dic} {6}
    \gantttitle{En} {6}
    \gantttitle{Feb} {6}
    \gantttitle{Mar} {6} \\

    \ganttgroup[inline = false] {Antecedentes} {1}{3} \\
    \ganttbar[inline = false] {Búsqueda y selección del tema del TFG} {1}{2} \\
    \ganttbar[inline = false] {Búsqueda y clasificación de documentación} {2}{3}

    \ganttnewline[dashed, black!50]

    \ganttgroup[inline = false] {Flujo de trabajo} {4}{7} \\
    \ganttbar[inline = false] {Instalación de las herramientas de diseño} {4}{5} \\
    \ganttbar[inline = false] {Configuración de las herramientas}{6}{7}

    \ganttnewline[dashed, black!50]

    \ganttgroup[inline = false] {Evaluación de la tecnología del nodo} {8}{9} \\
    \ganttbar[inline = false] {Evaluación de los transistores MOS} {8}{8} \\
    \ganttbar[inline = false] {Análisis de espejos de corriente} {9}{9} \\

    \ganttgroup[inline = false] {Simulación espejos de corriente} {10}{12} \\
    \ganttbar[inline = false] {Simulación DC} {10}{11} \\
    \ganttbar[inline = false] {Simulación AC} {11}{12}

    \ganttnewline[dashed, black!50]

    \ganttgroup[inline = false] {Especificaciones del amplificador} {13}{13} \\
    \ganttbar[inline = false] {Definición de rango de frecuencias de entrada} {13}{13} \\
    \ganttbar[inline = false] {Definición de ancho de banda} {13}{13} \\
    \ganttbar[inline = false] {Definición de consumo de energía} {13}{13} \\
    \ganttbar[inline = false] {Definición de características eléctricas} {13}{13}

    \ganttnewline[dashed, black!50]

    \ganttgroup[inline = false] {Definición de jerarquías} {14}{15} \\
    \ganttbar[inline = false] {Creación de símbolos} {14}{14} \\
    \ganttbar[inline = false] {Esquemático} {14}{15}

    \ganttnewline[dashed, black!50]

    \ganttgroup[inline = false] {Diseño del amplificador} {16}{21} \\
    \ganttbar[inline = false] {Etapa diferencial} {16}{19} \\
    \ganttbar[inline = false] {Etapa potencia} {17}{19} \\
    \ganttbar[inline = false] {Realimentación Miller} {19}{21} \\

    \ganttgroup[inline = false] {Simulación del amplificador} {19}{24} \\
    \ganttbar[inline = false] {Simulación DC} {19}{22} \\
    \ganttbar[inline = false] {Simulación AC} {21}{24} \\

    \ganttgroup[inline = false] {Layout} {25}{30} \\
    \ganttbar[inline = false] {Definición de tamaño de floorplan} {25}{25} \\
    \ganttbar[inline = false] {Definición de tamaño del módulo del capacitor} {25}{25} \\
    \ganttbar[inline = false] {Definición de ubicación de puertos} {25}{26} \\
    \ganttbar[inline = false] {Definición de uso de metales} {25}{26} \\
    \ganttbar[inline = false] {Creación de wells para cada dominio de voltaje} {26}{27} \\
    \ganttbar[inline = false] {Diseño de distribución para apareamiento de transistores} {25}{28} \\
    \ganttbar[inline = false] {Dibujado de transistores} {28}{30} \\

    \ganttgroup[inline = false] {Verificación física} {27}{33} \\
    \ganttbar[inline = false] {DRC} {27}{33} \\
    \ganttbar[inline = false] {Chequeo de Well y Substrato} {27}{33} \\
    \ganttbar[inline = false] {Chequeo de Antena} {27}{33} \\
    \ganttbar[inline = false] {NCC/LVS} {27}{33} \\

    \ganttgroup[inline = false] {Extracción de parásitos} {34}{34} \\
    \ganttbar[inline = false] {Configuración de extracción conservativa} {34}{34} \\
    \ganttbar[inline = false] {Anotado de parásitos en netlist} {34}{34} \\

    \ganttgroup[inline = false] {Simulación post-layout} {35}{35} \\
    \ganttbar[inline = false] {Análisis de potencia de ruido} {35}{35}

    \ganttnewline[dashed, black!50]

    \ganttgroup[inline = false] {Generación de fabricables} {36}{36} \\
    \ganttbar[inline = false] {Exportación de GDSII stream format} {36}{36}

    \ganttnewline[dashed, black!50]

    \ganttgroup[inline = false] {Anteproyecto} {37}{40} \\
    \ganttbar[inline = false] {Configuración de las herramientas para la documentación} {37}{38} \\
    \ganttbar[inline = false] {Creación del formato del documento} {38}{39} \\
    \ganttbar[inline = false] {Redacción} {37}{40} \\

    \ganttgroup[inline = false] {Informe Final} {37}{42} \\
    \ganttbar[inline = false] {Redacción} {37}{42}

  \end{ganttchart}
\end{center}


\newpage


# Bibliografía

[http://en.wikipedia.org/wiki/GDSII - GDSII (Graphic Database System) stream format](http://en.wikipedia.org/wiki/GDSII), última consulta 22/08/2019.
