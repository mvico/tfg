$ ls informe.md | entr -s "pandoc informe.md 
    ../recursos/formato/config.yaml 
    -o informe.pdf
    --template ~/.pandoc/templates/eisvogel.latex
    -H ../recursos/formato/preamble.tex
    --top-level-division=chapter 
    --listings
    --filter pandoc-eqnos
    --filter=pandoc-citeproc"
