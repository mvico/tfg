# Default layers naming on Electric's GDSII export

25/0: Active-Cut
41/0: P-Well
42/0: N-Well
43/0: 
44/0: P-Select
45/0: N-Active
46/0: Poly1 + Transistor-Poly
49/0: Metal-1 (M1)
50/0: Via1 (V1)
51/0: Metal-2 (M2)
52/0: 
56/0: Poly2?
61/0: 
62/0: 

# Layers z-height for GDSII 2.5D representation in KLayout

25/0: 0 1
41/0: 1 2
42/0: 2 3
43/0: 3 4
44/0: 4 5
45/0: 5 6
46/0: 6 7
49/0: 7 8
50/0: 8 9
51/0: 9 10
52/0: 10 11
56/0: 11 12
61/0: 12 13
62/0: 13 14

