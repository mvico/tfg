#!/bin/bash

# Get argument from electric
# Args ${USE_DIR} -i ${FILENAME_NO_EXT} -r ${FILENAME_NO_EXT}.raw -o ${FILENAME_NO_EXT}.out
# /Users/mvico/projects/tfg/recursos/OpAmp/sub_block_level -i PMOS_L -r PMOS_L.raw -o PMOS_L.out
args=("$@")

# Create *.net file for LTspice
cp ${args[0]}/${args[2]}.spi ${args[0]}/${args[2]}.net

echo ${args[1]}
echo ${args[0]}/${args[2]}.net
echo ${args[3]}
echo ${args[0]}/${args[4]}
echo ${args[5]}
echo ${args[0]}/${args[6]}

# Launch LTspice and pass it parameters
/Applications/LTspice.app/Contents/MacOS/LTSpice -b "${args[0]}/${args[2]}.net" ${args[3]} "${args[0]}/${args[4]}" ${args[5]} "${args[0]}/${args[6]}"
