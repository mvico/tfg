*** SPICE deck for cell opamp_chip_sim{sch} from library opamp
*** Created on lun dic 17, 2018 15:21:20
*** Last revised on vie feb 08, 2019 12:15:35
*** Written on vie feb 08, 2019 12:16:06 by Electric VLSI Design System, version 9.07
*** Layout tech: mocmos, foundry MOSIS
*** UC SPICE *** , MIN_RESIST 4.0, MIN_CAPAC 0.1FF

*** SUBCIRCUIT opamp__bias FROM CELL bias{sch}
.SUBCKT opamp__bias ibias vbias_a vbias_b vdd venable vss
Mnmos-4@6 net@37 net@37 net@42 vss NMOS L=0.9U W=10.5U
Mnmos-4@7 net@42 net@42 vss vss NMOS L=0.9U W=10.5U
Mnmos-4@8 ibias net@37 net@126 vss NMOS L=0.9U W=10.5U
Mnmos-4@9 net@126 net@42 vss vss NMOS L=0.9U W=10.5U
Mnmos-4@10 vbias_b venable net@193 vss NMOS L=0.6U W=3U
Mnmos-4@11 vbias_a venable net@198 vss NMOS L=0.6U W=3U
Mpmos-4@1 net@37 net@193 net@152 net@152 PMOS L=0.6U W=8.4U
Mpmos-4@5 net@152 net@198 vdd vdd PMOS L=0.6U W=8.4U
Mpmos-4@6 net@193 venable vdd vdd PMOS L=0.6U W=3U
Mpmos-4@7 net@198 venable vdd vdd PMOS L=0.6U W=3U
.ENDS opamp__bias

*** SUBCIRCUIT opamp__bias_x8 FROM CELL bias_x8{sch}
.SUBCKT opamp__bias_x8 ibias0 ibias1 ibias2 ibias3 ibias4 ibias5 ibias6 ibias7 iref vdd venable0 venable1 venable2 venable3 venable4 venable5 venable6 venable7 vss
Mpmos-4@0 iref iref vbias_a vbias_a PMOS L=0.6U W=8.4U
Mpmos-4@1 vbias_a vbias_a vdd vdd PMOS L=0.6U W=8.4U
Xbias@0 ibias0 vbias_a iref vdd venable0 vss opamp__bias
Xbias@1 ibias1 vbias_a iref vdd venable1 vss opamp__bias
Xbias@2 ibias2 vbias_a iref vdd venable2 vss opamp__bias
Xbias@3 ibias3 vbias_a iref vdd venable3 vss opamp__bias
Xbias@4 ibias4 vbias_a iref vdd venable4 vss opamp__bias
Xbias@5 ibias5 vbias_a iref vdd venable5 vss opamp__bias
Xbias@6 ibias6 vbias_a iref vdd venable6 vss opamp__bias
Xbias@7 ibias7 vbias_a iref vdd venable7 vss opamp__bias
.ENDS opamp__bias_x8

*** SUBCIRCUIT opamp__capacitor_Diego FROM CELL capacitor_Diego{sch}
.SUBCKT opamp__capacitor_Diego P1 P2

* Spice Code nodes in cell cell 'capacitor_Diego{sch}'
Cc P1 P2 3.3p
Ccp P1 GND 330f
.ENDS opamp__capacitor_Diego

*** SUBCIRCUIT opamp__opamp_Diego FROM CELL opamp_Diego{sch}
.SUBCKT opamp__opamp_Diego iref vdd vinn vinp vout vss
Mnmos-4@0 net@105 net@124 vss vss NMOS L=2.1U W=5.4U M=10
Mnmos-4@1 net@124 net@124 vss vss NMOS L=2.1U W=5.4U M=10
Mnmos-4@2 vout net@105 vss vss NMOS L=0.6U W=6.3U M=18
Mnmos-4@3 net@171 vdd net@105 vss NMOS L=0.6U W=4.2U M=1.0
Mpmos-4@0 net@10 iref vdd vdd PMOS L=1.5U W=12U M=2
Mpmos-4@1 iref iref vdd vdd PMOS L=1.5U W=12U M=1.0
Mpmos-4@2 vout iref vdd vdd PMOS L=1.5U W=12U M=15
Mpmos-4@3 net@124 vinn net@10 net@10 PMOS L=0.6U W=9U M=20
Mpmos-4@4 net@105 vinp net@10 net@10 PMOS L=0.6U W=9U M=20
Xcapacito@0 vout net@171 opamp__capacitor_Diego
.ENDS opamp__opamp_Diego

*** SUBCIRCUIT opamp__capacitor_Fabio FROM CELL capacitor_Fabio{sch}
.SUBCKT opamp__capacitor_Fabio P1 P2

* Spice Code nodes in cell cell 'capacitor_Fabio{sch}'
Cc P1 P2 5p
Ccp P1 GND 500f
.ENDS opamp__capacitor_Fabio

*** SUBCIRCUIT opamp__opamp_Fabio FROM CELL opamp_Fabio{sch}
.SUBCKT opamp__opamp_Fabio iref vdd vinn vinp vout vss
Mnmos-4@0 net@105 net@124 vss vss NMOS L=0.9U W=12.6U M=6
Mnmos-4@1 net@124 net@124 vss vss NMOS L=0.9U W=12.6U M=6
Mnmos-4@2 vout net@105 vss vss NMOS L=0.9U W=16.2U M=25
Mnmos-4@3 net@155 vdd net@105 vss NMOS L=0.6U W=9U M=1.0
Mpmos-4@0 net@10 iref vdd vdd PMOS L=0.6U W=12U M=3
Mpmos-4@1 iref iref vdd vdd PMOS L=0.6U W=12U M=1.0
Mpmos-4@2 vout iref vdd vdd PMOS L=0.6U W=12U M=9
Mpmos-4@3 net@124 vinn net@10 net@10 PMOS L=0.6U W=9U M=32
Mpmos-4@4 net@105 vinp net@10 net@10 PMOS L=0.6U W=9U M=32
Xcapacito@0 vout net@155 opamp__capacitor_Fabio
.ENDS opamp__opamp_Fabio

*** SUBCIRCUIT opamp__capacitor_Marcos FROM CELL capacitor_Marcos{sch}
.SUBCKT opamp__capacitor_Marcos P1 P2

* Spice Code nodes in cell cell 'capacitor_Marcos{sch}'
Cc P1 P2 6p
Ccp P1 GND 600f
.ENDS opamp__capacitor_Marcos

*** SUBCIRCUIT opamp__opamp_Marcos FROM CELL opamp_Marcos{sch}
.SUBCKT opamp__opamp_Marcos iref vdd vinn vinp vout vss
Mnmos-4@0 net@105 net@124 vss vss NMOS L=1.8U W=9U M=50
Mnmos-4@1 net@124 net@124 vss vss NMOS L=1.8U W=9U M=50
Mnmos-4@2 vout net@105 vss vss NMOS L=0.6U W=9U M=45
Mnmos-4@3 net@201 vdd net@105 vss NMOS L=0.6U W=15U M=1.0
Mpmos-4@0 net@10 iref vdd vdd PMOS L=0.6U W=9U M=6
Mpmos-4@1 iref iref vdd vdd PMOS L=0.6U W=9U M=1.0
Mpmos-4@2 vout iref vdd vdd PMOS L=0.6U W=9U M=30
Mpmos-4@3 net@124 vinn net@10 net@10 PMOS L=0.9U W=9U M=30
Mpmos-4@4 net@105 vinp net@10 net@10 PMOS L=0.9U W=9U M=30
Xcapacito@0 vout net@201 opamp__capacitor_Marcos
.ENDS opamp__opamp_Marcos

*** SUBCIRCUIT opamp__capacitor_Matias FROM CELL capacitor_Matias{sch}
.SUBCKT opamp__capacitor_Matias P1 P2

* Spice Code nodes in cell cell 'capacitor_Matias{sch}'
Cc P1 P2 2.5p
Ccp P1 GND 250f
.ENDS opamp__capacitor_Matias

*** SUBCIRCUIT opamp__opamp_Matias FROM CELL opamp_Matias{sch}
.SUBCKT opamp__opamp_Matias iref vdd vinn vinp vout vss
Mnmos-4@0 net@105 net@124 vss vss NMOS L=2.4U W=5.1U M=15
Mnmos-4@1 net@124 net@124 vss vss NMOS L=2.4U W=5.1U M=15
Mnmos-4@2 vout net@105 vss vss NMOS L=0.6U W=10.2U M=60
Mnmos-4@3 net@208 vdd net@105 vss NMOS L=1.5U W=3U M=5
Mpmos-4@0 net@10 iref vdd vdd PMOS L=0.6U W=4.8U M=3
Mpmos-4@1 iref iref vdd vdd PMOS L=0.6U W=4.8U M=2
Mpmos-4@2 vout iref vdd vdd PMOS L=0.6U W=4.8U M=50
Mpmos-4@3 net@124 vinn net@10 net@10 PMOS L=0.6U W=18U M=20
Mpmos-4@4 net@105 vinp net@10 net@10 PMOS L=0.6U W=18U M=20
Xpoly_cap@0 vout net@208 opamp__capacitor_Matias
.ENDS opamp__opamp_Matias

*** SUBCIRCUIT opamp__esd_nwell FROM CELL esd_nwell{sch}
.SUBCKT opamp__esd_nwell N_well P_act

* Spice Code nodes in cell cell 'esd_nwell{sch}'
D2 P_act N_well Dpnp
.model Dpnp D is=1e-18 n=1
.ENDS opamp__esd_nwell

*** SUBCIRCUIT opamp__esd_pwell FROM CELL esd_pwell{sch}
.SUBCKT opamp__esd_pwell N_act P_well

* Spice Code nodes in cell cell 'esd_pwell{sch}'
D1 P_well N_act Dnpn
.model Dnpn D is=1e-18 n=1
.ENDS opamp__esd_pwell

*** SUBCIRCUIT opamp__vdd_gnd_bus FROM CELL vdd_gnd_bus{sch}
.SUBCKT opamp__vdd_gnd_bus gnd vdd

* Spice Code nodes in cell cell 'vdd_gnd_bus{sch}'
*R1 vdd_1 gnd_1 10g
.ENDS opamp__vdd_gnd_bus

*** SUBCIRCUIT opamp__pad_analog FROM CELL pad_analog{sch}
.SUBCKT opamp__pad_analog Analog gnd vdd
Xesd_nwel@0 vdd Analog opamp__esd_nwell
Xesd_pwel@0 Analog gnd opamp__esd_pwell
Xvdd_gnd_@0 gnd vdd opamp__vdd_gnd_bus
.ENDS opamp__pad_analog

*** SUBCIRCUIT opamp__pad_gnd FROM CELL pad_gnd{sch}
.SUBCKT opamp__pad_gnd gnd vdd
Xesd_nwel@0 vdd gnd opamp__esd_nwell
Xesd_pwel@0 gnd gnd opamp__esd_pwell
Xvdd_gnd_@0 gnd vdd opamp__vdd_gnd_bus
.ENDS opamp__pad_gnd

*** SUBCIRCUIT opamp__pad_vdd FROM CELL pad_vdd{sch}
.SUBCKT opamp__pad_vdd gnd vdd
Xesd_nwel@0 vdd vdd opamp__esd_nwell
Xesd_pwel@0 vdd gnd opamp__esd_pwell
Xvdd_gnd_@0 gnd vdd opamp__vdd_gnd_bus
.ENDS opamp__pad_vdd

*** SUBCIRCUIT opamp__padframe FROM CELL padframe{sch}
.SUBCKT opamp__padframe gnd Pin1 Pin11 Pin12 Pin13 Pin14 Pin15 Pin16 Pin17 Pin18 Pin20 Pin21 Pin22 Pin24 Pin25 Pin26 Pin28 Pin29 Pin3 Pin30 Pin31 Pin32 Pin33 Pin35 Pin36 Pin37 Pin39 Pin4 Pin40 Pin5 Pin6 Pin7 Pin8 Pin9 vdd
Xpad_anal@0 Pin12 gnd vdd opamp__pad_analog
Xpad_anal@1 Pin13 gnd vdd opamp__pad_analog
Xpad_anal@2 Pin14 gnd vdd opamp__pad_analog
Xpad_anal@3 Pin15 gnd vdd opamp__pad_analog
Xpad_anal@4 Pin6 gnd vdd opamp__pad_analog
Xpad_anal@5 Pin7 gnd vdd opamp__pad_analog
Xpad_anal@6 Pin8 gnd vdd opamp__pad_analog
Xpad_anal@7 Pin9 gnd vdd opamp__pad_analog
Xpad_anal@8 Pin21 gnd vdd opamp__pad_analog
Xpad_anal@9 Pin22 gnd vdd opamp__pad_analog
Xpad_anal@10 Pin24 gnd vdd opamp__pad_analog
Xpad_anal@11 Pin25 gnd vdd opamp__pad_analog
Xpad_anal@12 Pin16 gnd vdd opamp__pad_analog
Xpad_anal@13 Pin17 gnd vdd opamp__pad_analog
Xpad_anal@14 Pin18 gnd vdd opamp__pad_analog
Xpad_anal@16 Pin31 gnd vdd opamp__pad_analog
Xpad_anal@17 Pin32 gnd vdd opamp__pad_analog
Xpad_anal@18 Pin33 gnd vdd opamp__pad_analog
Xpad_anal@19 Pin35 gnd vdd opamp__pad_analog
Xpad_anal@20 Pin26 gnd vdd opamp__pad_analog
Xpad_anal@21 Pin30 gnd vdd opamp__pad_analog
Xpad_anal@22 Pin28 gnd vdd opamp__pad_analog
Xpad_anal@23 Pin29 gnd vdd opamp__pad_analog
Xpad_anal@24 Pin1 gnd vdd opamp__pad_analog
Xpad_anal@25 Pin3 gnd vdd opamp__pad_analog
Xpad_anal@26 Pin4 gnd vdd opamp__pad_analog
Xpad_anal@27 Pin5 gnd vdd opamp__pad_analog
Xpad_anal@28 Pin36 gnd vdd opamp__pad_analog
Xpad_anal@29 Pin37 gnd vdd opamp__pad_analog
Xpad_anal@30 Pin39 gnd vdd opamp__pad_analog
Xpad_anal@31 Pin40 gnd vdd opamp__pad_analog
Xpad_anal@33 Pin11 gnd vdd opamp__pad_analog
Xpad_anal@34 Pin20 gnd vdd opamp__pad_analog
Xpad_gnd@1 gnd vdd opamp__pad_gnd
Xpad_gnd@2 gnd vdd opamp__pad_gnd
Xpad_gnd@3 gnd vdd opamp__pad_gnd
Xpad_vdd@0 gnd vdd opamp__pad_vdd
Xpad_vdd@1 gnd vdd opamp__pad_vdd
Xpad_vdd@2 gnd vdd opamp__pad_vdd
Xpad_vdd@3 gnd vdd opamp__pad_vdd
.ENDS opamp__padframe

*** SUBCIRCUIT opamp__opamp_chip FROM CELL opamp_chip{sch}
.SUBCKT opamp__opamp_chip gnd iref vdd venable0 venable1 venable2 venable3 venable4 venable5 venable6 venable7 vin0 vin1 vin2 vin3 vin4 vin5 vin6 vin7 vip0 vip1 vip2 vip3 vip4 vip5 vip6 vip7 vout0 vout1 vout2 vout3 vout4 vout5 vout6 vout7
Xbias_x8@0 ibias0 ibias1 ibias2 ibias3 ibias4 ibias5 ibias6 ibias7 iref vdd venable0 venable1 venable2 venable3 venable4 venable5 venable6 venable7 gnd opamp__bias_x8
Xopamp_Di@0 ibias0 vdd vin0 vip0 vout0 gnd opamp__opamp_Diego
Xopamp_Di@7 ibias7 vdd vin7 vip7 vout7 gnd opamp__opamp_Diego
Xopamp_Fa@0 ibias2 vdd vin2 vip2 vout2 gnd opamp__opamp_Fabio
Xopamp_Fa@1 ibias5 vdd vin5 vip5 vout5 gnd opamp__opamp_Fabio
Xopamp_Ma@0 ibias1 vdd vin1 vip1 vout1 gnd opamp__opamp_Marcos
Xopamp_Ma@1 ibias6 vdd vin6 vip6 vout6 gnd opamp__opamp_Marcos
Xopamp_Ma@2 ibias4 vdd vin4 vip4 vout4 gnd opamp__opamp_Matias
Xopamp_Ma@3 ibias3 vdd vin3 vip3 vout3 gnd opamp__opamp_Matias
Xpadframe@1 gnd vin6 venable3 venable2 venable1 venable0 iref vout0 vip0 vin0 vin1 vip1 vout1 vip2 vin2 vout2 vip3 vin3 vin7 vout3 vout4 vin4 vip4 vout5 vin5 vip5 vout6 vip7 vip6 vout7 venable7 venable6 venable5 venable4 vdd opamp__padframe
.ENDS opamp__opamp_chip

.global gnd

*** TOP LEVEL CELL: opamp_chip_sim{sch}
Xopamp_ch@0 gnd iref vdd venable0 venable1 venable2 venable3 venable4 venable5 venable6 venable7 ve0 ve1 ve2 ve3 ve4 ve5 ve6 ve7 vcm vcm vcm vcm vcm vcm vcm vcm vout0 vout1 vout2 vout3 vout4 vout5 vout6 vout7 opamp__opamp_chip

* Spice Code nodes in cell cell 'opamp_chip_sim{sch}'
vdd vdd 0 DC 5
Rref iref 0 20.5k
venable0 venable0 0 DC 5
venable1 venable1 0 DC 5
venable2 venable2 0 DC 5
venable3 venable3 0 DC 5
venable4 venable4 0 DC 5
venable5 venable5 0 DC 5
venable6 venable6 0 DC 5
venable7 venable7 0 DC 5
vin vin 0 DC=0V AC=100mV SIN (2.5V 100mV 1MEG 1ns)
vcm vcm 0 DC 2.5
** * OpAmp 0 ***
Cin0 vin vr0 1
Rin0 vr0 ve0 500
Ce0 ve0 0 5p
Rf0 ve0 vout0 5k
Cout0 vout0 0 20p
** * OpAmp 1 ***
Cin1 vin vr1 1
Rin1 vr1 ve1 500
Ce1 ve1 0 5p
Rf1 ve1 vout1 5k
Cout1 vout1 0 20p
** * OpAmp 2 ***
Cin2 vin vr2 1
Rin2 vr2 ve2 500
Ce2 ve2 0 5p
Rf2 ve2 vout2 5k
Cout2 vout2 0 20p
** * OpAmp 3 ***
Cin3 vin vr3 1
Rin3 vr3 ve3 500
Ce3 ve3 0 5p
Rf3 ve3 vout3 5k
Cout3 vout3 0 20p
** * OpAmp 4 ***
Cin4 vin vr4 1
Rin4 vr4 ve4 500
Ce4 ve4 0 5p
Rf4 ve4 vout4 5k
Cout4 vout4 0 20p
** * OpAmp 5 ***
Cin5 vin vr5 1
Rin5 vr5 ve5 500
Ce5 ve5 0 5p
Rf5 ve5 vout5 5k
Cout5 vout5 0 20p
** * OpAmp 6 ***
Cin6 vin vr6 1
Rin6 vr6 ve6 500
Ce6 ve6 0 5p
Rf6 ve6 vout6 5k
Cout6 vout6 0 20p
** * OpAmp 0 ***
Cin7 vin vr7 1
Rin7 vr7 ve7 500
Ce7 ve7 0 5p
Rf7 ve7 vout7 5k
Cout7 vout7 0 20p
* .tran 0 11u
.ac dec 1K 100 10G
.include /Users/mvico/tfg/recursos/OpAmp/tecnologia/C5_models.txt
.END
