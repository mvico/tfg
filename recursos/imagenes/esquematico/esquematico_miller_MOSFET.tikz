\begin{circuitikz}[american voltages, scale = 2]
  \draw[thick]
    % Transistor M5
    (0,0) node[pmos, bulk, solderdot, emptycircle] (mos5) {}
    (mos5.G) node[anchor = south] {$M_{5}$}
    (mos5.G) node[anchor = east] {...}
    (mos5.S) node[vdd] (power) {}
    (mos5.bulk) -| (mos5.S)
    (mos5.inner up) node[circ] {}

    (mos5.D) ++(+0.5,-0.5) node[pmos, bulk, solderdot, emptycircle, xscale = -1] (mos2) {}

    (mos5.D) -| (mos2.S) 

    % Transistor M2
    (mos2.bulk) node[anchor = east] {$M_{2}$}
    (mos2.G) node[anchor = west] {$V_{in+}$}
    (mos2.G) node[circle, inner sep = 1pt, fill = white, draw] {}
    (mos2.bulk) -| (mos2.S)
    (mos2.inner up) node[circ] {}

    (mos2.S) ++(0,-2.5) node[nmos, bulk, solderdot, emptycircle] (mos4) {}
    (mos2.D) -- (mos4.D)
    (mos4.bulk) -| (mos4.S)
    (mos4.inner down) node[circ] {}


    % Transistor M4
    (mos4.bulk) node[anchor = west] {$M_{4}$}
    (mos4.G) node[anchor = east] {...}
    (mos4.S) node[rground] (ground) {}

    % Transistor M7
    (3,0) node[pmos, bulk, solderdot, emptycircle] (mos7) {}
    (mos7.G) node[anchor = south] {$M_{7}$}
    (mos7.S) node[vdd] (power) {}
    (mos7.bulk) -| (mos7.S)
    (mos7.inner up) node[circ] {}

    (mos7.D) ++(0,-2.5) node[nmos, bulk, solderdot, emptycircle] (mos6) {}
    (mos7.G) -- (mos5.G)
    (mos7.D) -- (mos6.D) node[midway] (mid_to_M6) {} 
    (mos7.bulk) -| (mos7.S)
    (mos7.inner up) node[circ] {}


    % Transistor M6
    (mos6.G) node[anchor = south] {$M_{6}$}
    (mos6.S) node[rground] (ground) {}
    (mos6.bulk) -| (mos6.S)
    (mos6.inner down) node[circ] {}


    % Red Miller
    (mid_to_M6) to [C, l = $C_{C}$, n = c_miller] ++(-0.5,0) to node[nmos, bulk, solderdot, emptycircle, anchor=D, rotate=-90] (mosR) {} ++(-0.5,0)
    (mosR.G) node[anchor=west] {$M_{Rc}$}
    (mosR.S) |- (mos6.G) node[midway] (mid) {}
    (mosR.S) -| (mos2.D) node[midway] (mid_to_M2) {}
    (mosR.G) |- (mos7.G) node[midway] (mid_to_M7) {}
    (mosR.S) node[circ] {}
    (mid_to_M2) node[circ] {}
    (mid_to_M6) node[circ] {}
    (mid_to_M7) node[circ] {}
    (mosR.bulk) -- ++(0,-0.5) node[rground] {}
    ;
\end{circuitikz}
