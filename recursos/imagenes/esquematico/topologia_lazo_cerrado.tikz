\begin{circuitikz}[american voltages, scale = 2]
  \draw[thick]
    (0,0) node[op amp] (opamp) {}
    (opamp.-) to[short] ++(-1,0)
    to[R, l=$\SI{500}{\ohm}$] ++(-1,0)
    to[C, l=$\SI{1}{\farad}$] ++(-1,0)
    to[short] ++(-0.5,0)
    to[short] ++(0,-0.5)
    to[sV, l_=$V_{in\;(AC)}$] ++(0,-0.5)
    to[short] ++(0,-0.5) coordinate (between_voltages)
    to[short] ++(0,-0.25) 
    to[V, l_=$V_{CM\;(DC)}$, invert] ++(0,-1)
    node[rground] {}


    (opamp.+) to[short] ++(-0.5,0) coordinate (opampp)
    (opampp) |- (between_voltages)
    (between_voltages) node[circ] {}

    (opamp.out) to[short] ++(1,0)
    to[C, l=$C_{L} \equal \SI{20}{\pico \farad}$] ++(0,-1)
    node[rground] {}

    (opamp.-) to[short] ++(-0.5,0)
    to[short,*-] ++(0,1)
    to[R, l=$\SI{5000}{\ohm}$] ++(2,0) coordinate (rightR)

    (opamp.out) -- ++(0.5,0) node[circ] (output) {}

    (rightR) -| (output)
    ;
\end{circuitikz}
