#!/bin/bash

# Making the aliases work inside the script
shopt -s expand_aliases

# Plotter
alias wv='$HOME/projects/WaveformViewer/waveform/wv.py'

# Path a las imágenes
path="sim"

# Características comunes de las imágenes
# width=650
# height=$width
# width=500
# height=300
width=1000
height=600
labels_size=18
line_width=2
sep="\t"

# Configuración de salida
# flags='--save_png --save_svg --no_output -hi 1 -hi 1 -dp'
flags="--save_png --save_svg --no_output"

# OP {{{ 
## NMOS {{{
### Group typical {{{
wv -i ${path}/punto-de-operacion/data/NMOS_punto-de-operacion_VGS_group.csv -o ${path}/punto-de-operacion/images/NMOS_punto-de-operacion_VGS_group.png -xl '$V_{GS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -nl -dp -pm --width $width --height $height -ls $labels_size -sep $sep --use_scientific --regression cuadratic -hi 1 -xr -0.2 5.5 -yr -0.00025 0.0045 $flags
inkscape -D ${path}/punto-de-operacion/images/NMOS_punto-de-operacion_VGS_group.svg -o ${path}/punto-de-operacion/images/NMOS_punto-de-operacion_VGS_group.pdf --export-latex
### }}}
### Corners {{{
wv -i ${path}/punto-de-operacion/data/NMOS_punto-de-operacion_65_5.csv -o ${path}/punto-de-operacion/images/NMOS_punto-de-operacion_corners_0p6.png -xl '$V_{GS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -nl -dp -pm --width $width --height $height -ls $labels_size -sep $sep --use_scientific -s "Id(Mn_l_0p6)" -ia ${path}/punto-de-operacion/data/NMOS_punto-de-operacion_125_4p5.csv -sa "Id(Mn_l_0p6)" -ib ${path}/punto-de-operacion/data/NMOS_punto-de-operacion_0_5p5.csv -sb "Id(Mn_l_0p6)" --header_index 0 -dp -nl -xr -0.2 5.5 -yr -0.00025 0.0045 -pm $flags
inkscape -D ${path}/punto-de-operacion/images/NMOS_punto-de-operacion_corners_0p6.svg -o ${path}/punto-de-operacion/images/NMOS_punto-de-operacion_corners_0p6.pdf --export-latex
wv -i ${path}/punto-de-operacion/data/NMOS_punto-de-operacion_65_5.csv -o ${path}/punto-de-operacion/images/NMOS_punto-de-operacion_corners_3p0.png -xl '$V_{GS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -nl -dp -pm --width $width --height $height -ls $labels_size -sep $sep --use_scientific -s "Id(Mn_l_3p0)" -ia ${path}/punto-de-operacion/data/NMOS_punto-de-operacion_125_4p5.csv -sa "Id(Mn_l_3p0)" -ib ${path}/punto-de-operacion/data/NMOS_punto-de-operacion_0_5p5.csv -sb "Id(Mn_l_3p0)" --header_index 0 -dp -nl -xr -0.2 5.5 -yr -0.00025 0.0045 -pm $flags
inkscape -D ${path}/punto-de-operacion/images/NMOS_punto-de-operacion_corners_3p0.svg -o ${path}/punto-de-operacion/images/NMOS_punto-de-operacion_corners_3p0.pdf --export-latex
wv -i ${path}/punto-de-operacion/data/NMOS_punto-de-operacion_65_5.csv -o ${path}/punto-de-operacion/images/NMOS_punto-de-operacion_corners_6p0.png -xl '$V_{GS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -nl -dp -pm --width $width --height $height -ls $labels_size -sep $sep --use_scientific -s "Id(Mn_l_6p0)" -ia ${path}/punto-de-operacion/data/NMOS_punto-de-operacion_125_4p5.csv -sa "Id(Mn_l_6p0)" -ib ${path}/punto-de-operacion/data/NMOS_punto-de-operacion_0_5p5.csv -sb "Id(Mn_l_6p0)" --header_index 0 -dp -nl -xr -0.2 5.5 -yr -0.00025 0.0045 -pm $flags
inkscape -D ${path}/punto-de-operacion/images/NMOS_punto-de-operacion_corners_6p0.svg -o ${path}/punto-de-operacion/images/NMOS_punto-de-operacion_corners_6p0.pdf --export-latex
### }}}
## }}}
## PMOS {{{
### Group typical {{{
wv -i ${path}/punto-de-operacion/data/PMOS_punto-de-operacion_VGS_group.csv -o ${path}/punto-de-operacion/images/PMOS_punto-de-operacion_VGS_group.png -xl '$V_{GS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -nl -xr -5.5 0.2 -yr -0.0015 0.00025 -dp -pm --width $width --height $height -ls $labels_size -sep $sep --use_scientific --regression cuadratic -hi 1 $flags
inkscape -D ${path}/punto-de-operacion/images/PMOS_punto-de-operacion_VGS_group.svg -o ${path}/punto-de-operacion/images/PMOS_punto-de-operacion_VGS_group.pdf --export-latex
### }}}
### Corners {{{
wv -i ${path}/punto-de-operacion/data/PMOS_punto-de-operacion_65_5.csv -o ${path}/punto-de-operacion/images/PMOS_punto-de-operacion_corners_0p6.png -xl '$V_{GS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -sep $sep --use_scientific -s "Id(Mp_l_0p6)" -ia ${path}/punto-de-operacion/data/PMOS_punto-de-operacion_125_4p5.csv -sa "Id(Mp_l_0p6)" -ib ${path}/punto-de-operacion/data/PMOS_punto-de-operacion_0_5p5.csv -sb "Id(Mp_l_0p6)" --header_index 0 -dp -nl -xr -5.5 0.2 -yr -0.0015 0.00025 -pm $flags
inkscape -D ${path}/punto-de-operacion/images/PMOS_punto-de-operacion_corners_0p6.svg -o ${path}/punto-de-operacion/images/PMOS_punto-de-operacion_corners_0p6.pdf --export-latex

wv -i ${path}/punto-de-operacion/data/PMOS_punto-de-operacion_65_5.csv -o ${path}/punto-de-operacion/images/PMOS_punto-de-operacion_corners_3p0.png -xl '$V_{GS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -sep $sep --use_scientific -s "Id(Mp_l_3p0)" -ia ${path}/punto-de-operacion/data/PMOS_punto-de-operacion_125_4p5.csv -sa "Id(Mp_l_3p0)" -ib ${path}/punto-de-operacion/data/PMOS_punto-de-operacion_0_5p5.csv -sb "Id(Mp_l_3p0)" --header_index 0 -dp -nl -xr -5.5 0.2 -yr -0.0015 0.00025 -pm $flags
inkscape -D ${path}/punto-de-operacion/images/PMOS_punto-de-operacion_corners_3p0.svg -o ${path}/punto-de-operacion/images/PMOS_punto-de-operacion_corners_3p0.pdf --export-latex

wv -i ${path}/punto-de-operacion/data/PMOS_punto-de-operacion_65_5.csv -o ${path}/punto-de-operacion/images/PMOS_punto-de-operacion_corners_6p0.png -xl '$V_{GS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -sep $sep --use_scientific -s "Id(Mp_l_6p0)" -ia ${path}/punto-de-operacion/data/PMOS_punto-de-operacion_125_4p5.csv -sa "Id(Mp_l_6p0)" -ib ${path}/punto-de-operacion/data/PMOS_punto-de-operacion_0_5p5.csv -sb "Id(Mp_l_6p0)" --header_index 0 -dp -nl -xr -5.5 0.2 -yr -0.0015 0.00025 -pm $flags
inkscape -D ${path}/punto-de-operacion/images/PMOS_punto-de-operacion_corners_6p0.svg -o ${path}/punto-de-operacion/images/PMOS_punto-de-operacion_corners_6p0.pdf --export-latex
### }}}
## }}}
# }}}
# ID vs VGS {{{ 
## VGS lineal, ID lineal {{{ 
### NMOS {{{
# wv -i ${path}/ID_vs_VGS/data/NMOS_ID_vs_VGS_group.csv -o ${path}/ID_vs_VGS/images/NMOS_ID_vs_VGS_group.png -xl '$V_{GS}\,[\volt]$' -yl '$I_{D}\,[\ampere]$' -lw $line_width -pm --width $width --height $height -ls $labels_size --dont_use_scientific -sep $sep -hi 1 $flags
wv -i ${path}/ID_vs_VGS/data/NMOS_ID_vs_VGS_group.csv -o ${path}/ID_vs_VGS/images/NMOS_ID_vs_VGS_group.png -xl '$V_{GS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -sep $sep --use_scientific --regression cuadratic -hi 1 $flags
inkscape -D ${path}/ID_vs_VGS/images/NMOS_ID_vs_VGS_group.svg -o ${path}/ID_vs_VGS/images/NMOS_ID_vs_VGS_group.pdf --export-latex
### }}}
### PMOS {{{
### }}}
## }}}
## VGS lineal, ID log {{{ 
### NMOS {{{
wv -i ${path}/ID_vs_VGS/data/NMOS_ID_vs_VGS_group.csv -o ${path}/ID_vs_VGS/images/NMOS_ID-log_vs_VGS_group.png -xl '$V_{GS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size --y_axis_log -sep $sep --regression cuadratic -hi 1 $flags
inkscape -D ${path}/ID_vs_VGS/images/NMOS_ID-log_vs_VGS_group.svg -o ${path}/ID_vs_VGS/images/NMOS_ID-log_vs_VGS_group.pdf --export-latex
### }}}
### PMOS {{{
### }}}
## }}}
# }}}
# {{{ ID vs VDS @ swp VGS
## {{{ L = 0,6 µm
# labels_size=30

### NMOS
wv -i ${path}/ID_vs_VDS_swp_VGS/data/NMOS_ID_vs_VDS_swp_VGS_0p6.csv -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_swp_VGS_0p6.png -xl '$V_{DS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -yr 0 0.004 -sep $sep -hi 1 $flags
inkscape -D ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_swp_VGS_0p6.svg -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_swp_VGS_0p6.pdf --export-latex

### PMOS
## }}}
## {{{ L = 3 µm
### NMOS
wv -i ${path}/ID_vs_VDS_swp_VGS/data/NMOS_ID_vs_VDS_swp_VGS_3p0.csv -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_swp_VGS_3p0.png -xl '$V_{DS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -yr 0 0.004 -sep $sep -hi 1 $flags
inkscape -D ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_swp_VGS_3p0.svg -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_swp_VGS_3p0.pdf --export-latex

### PMOS
## }}}
## {{{ L = 6 µm
### NMOS
wv -i ${path}/ID_vs_VDS_swp_VGS/data/NMOS_ID_vs_VDS_swp_VGS_6p0.csv -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_swp_VGS_6p0.png -xl '$V_{DS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -yr 0 0.004 -sep $sep -hi 1 $flags
inkscape -D ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_swp_VGS_6p0.svg -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_swp_VGS_6p0.pdf --export-latex

### PMOS
## }}}
# }}}
# {{{ gm, gm/ID
## {{{ gm/ID vs VGS
wv -i ${path}/gmID_vs_ID/data/NMOS_gmID_vs_VGS_0p6.csv -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_VGS_0p6.png -xl '$V_{GS}\,[V]$' -yl '$gm/I_{D}$' -lw $line_width -pm --width $width --height $height -ls $labels_size -sep $sep -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gmID_vs_VGS_0p6.svg -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_VGS_0p6.pdf --export-latex

wv -i ${path}/gmID_vs_ID/data/NMOS_gmID_vs_VGS_3p0.csv -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_VGS_3p0.png -xl '$V_{GS}\,[V]$' -yl '$gm/I_{D}$' -lw $line_width -pm --width $width --height $height -ls $labels_size -sep $sep -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gmID_vs_VGS_3p0.svg -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_VGS_3p0.pdf --export-latex

wv -i ${path}/gmID_vs_ID/data/NMOS_gmID_vs_VGS_6p0.csv -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_VGS_6p0.png -xl '$V_{GS}\,[V]$' -yl '$gm/I_{D}$' -lw $line_width -pm --width $width --height $height -ls $labels_size -sep $sep -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gmID_vs_VGS_6p0.svg -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_VGS_6p0.pdf --export-latex

wv -i ${path}/gmID_vs_ID/data/NMOS_gmID_vs_VGS_group.csv -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_VGS_group.png -xl '$V_{GS}\,[V]$' -yl '$gm/I_{D}$' -lw $line_width -pm --width $width --height $height -ls $labels_size -sep $sep -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gmID_vs_VGS_group.svg -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_VGS_group.pdf --export-latex
## }}}
## {{{ gm vs ID
wv -i ${path}/gmID_vs_ID/data/NMOS_gm_vs_ID_0p6.csv -o ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID_0p6.png -xl '$I_{D}\,[A]$' -yl '$gm$' -lw $line_width -pm --width $width --height $height -ls $labels_size -xr 0 0.003 -yr 0 0.0013 -sep $sep -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID_0p6.svg -o ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID_0p6.pdf --export-latex
wv -i ${path}/gmID_vs_ID/data/NMOS_gm_vs_ID_3p0.csv -o ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID_3p0.png -xl '$I_{D}\,[A]$' -yl '$gm$' -lw $line_width -pm --width $width --height $height -ls $labels_size -xr 0 0.003 -yr 0 0.0013 -sep $sep -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID_3p0.svg -o ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID_3p0.pdf --export-latex
wv -i ${path}/gmID_vs_ID/data/NMOS_gm_vs_ID_6p0.csv -o ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID_6p0.png -xl '$I_{D}\,[A]$' -yl '$gm$' -lw $line_width -pm --width $width --height $height -ls $labels_size -xr 0 0.003 -yr 0 0.0013 -sep $sep -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID_6p0.svg -o ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID_6p0.pdf --export-latex
## }}}
## {{{ gm vs ID log
wv -i ${path}/gmID_vs_ID/data/NMOS_gm_vs_ID_0p6.csv -o ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID-log_0p6.png -xl '$I_{D}\,[A]$' -yl '$gm$' -lw $line_width -pm --width $width --height $height -ls $labels_size --x_axis_log -sep $sep --use_scientific -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID-log_0p6.svg -o ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID-log_0p6.pdf --export-latex
wv -i ${path}/gmID_vs_ID/data/NMOS_gm_vs_ID_3p0.csv -o ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID-log_3p0.png -xl '$I_{D}\,[A]$' -yl '$gm$' -lw $line_width -pm --width $width --height $height -ls $labels_size --x_axis_log -sep $sep --use_scientific -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID-log_3p0.svg -o ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID-log_3p0.pdf --export-latex
wv -i ${path}/gmID_vs_ID/data/NMOS_gm_vs_ID_6p0.csv -o ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID-log_6p0.png -xl '$I_{D}\,[A]$' -yl '$gm$' -lw $line_width -pm --width $width --height $height -ls $labels_size --x_axis_log -sep $sep --use_scientific -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID-log_6p0.svg -o ${path}/gmID_vs_ID/images/NMOS_gm_vs_ID-log_6p0.pdf --export-latex
## }}}
## {{{ gm/ID vs ID
# wv -i ${path}/gmID_vs_ID/data/gmID_vs_ID.csv -o ${path}/gmID_vs_ID/images/gmID_vs_ID.png -xl ID -yl gmID -lw $line_width -pm --width $width --height $height -ls $labels_size --dont_use_scientific -sep $sep -hi 1 $flags
wv -i ${path}/gmID_vs_ID/data/NMOS_gmID_vs_ID_0p6.csv -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID_0p6.png -xl '$I_{D}\,[A]$' -yl '$gm/I_{D}$' -lw $line_width -pm --width $width --height $height -ls $labels_size -xr 0 0.0015 -sep $sep -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID_0p6.svg -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID_0p6.pdf --export-latex
wv -i ${path}/gmID_vs_ID/data/NMOS_gmID_vs_ID_3p0.csv -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID_3p0.png -xl '$I_{D}\,[A]$' -yl '$gm/I_{D}$' -lw $line_width -pm --width $width --height $height -ls $labels_size -xr 0 0.0015 -sep $sep -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID_3p0.svg -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID_3p0.pdf --export-latex
wv -i ${path}/gmID_vs_ID/data/NMOS_gmID_vs_ID_6p0.csv -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID_6p0.png -xl '$I_{D}\,[A]$' -yl '$gm/I_{D}$' -lw $line_width -pm --width $width --height $height -ls $labels_size -xr 0 0.0015 -sep $sep -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID_6p0.svg -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID_6p0.pdf --export-latex
## }}}
## {{{ gm/ID vs ID log
wv -i ${path}/gmID_vs_ID/data/NMOS_gmID_vs_ID_0p6.csv -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID-log_0p6.png -xl '$I_{D}\,[A]$' -yl '$gm/I_{D}$' -lw $line_width -pm --width $width --height $height -ls $labels_size --x_axis_log -sep $sep --use_scientific -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID-log_0p6.svg -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID-log_0p6.pdf --export-latex
wv -i ${path}/gmID_vs_ID/data/NMOS_gmID_vs_ID_3p0.csv -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID-log_3p0.png -xl '$I_{D}\,[A]$' -yl '$gm/I_{D}$' -lw $line_width -pm --width $width --height $height -ls $labels_size --x_axis_log -sep $sep --use_scientific -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID-log_3p0.svg -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID-log_3p0.pdf --export-latex
wv -i ${path}/gmID_vs_ID/data/NMOS_gmID_vs_ID_6p0.csv -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID-log_6p0.png -xl '$I_{D}\,[A]$' -yl '$gm/I_{D}$' -lw $line_width -pm --width $width --height $height -ls $labels_size --x_axis_log -sep $sep --use_scientific -hi 1 $flags
inkscape -D ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID-log_6p0.svg -o ${path}/gmID_vs_ID/images/NMOS_gmID_vs_ID-log_6p0.pdf --export-latex
## }}}
# }}}
# {{{ ID vs VDS (grupo) swap VGS
# Imágenes ID vs VDS (grupo) @ VGS = 0 V
wv -i ${path}/ID_vs_VDS_swp_VGS/data/NMOS_ID_vs_VDS_para_VGS_0V.csv -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_0V.png -xl '$V_{DS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -yr 0 0.004 -sep $sep -hi 1 $flags
inkscape -D ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_0V.svg -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_0V.pdf --export-latex
# Imágenes ID vs VDS (grupo) @ VGS = 1 V
wv -i ${path}/ID_vs_VDS_swp_VGS/data/NMOS_ID_vs_VDS_para_VGS_1V.csv -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_1V.png -xl '$V_{DS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -yr 0 0.004 -sep $sep -hi 1 $flags
inkscape -D ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_1V.svg -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_1V.pdf --export-latex
# Imágenes ID vs VDS (grupo) @ VGS = 2 V
wv -i ${path}/ID_vs_VDS_swp_VGS/data/NMOS_ID_vs_VDS_para_VGS_2V.csv -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_2V.png -xl '$V_{DS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -yr 0 0.004 -sep $sep -hi 1 $flags
inkscape -D ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_2V.svg -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_2V.pdf --export-latex
# Imágenes ID vs VDS (grupo) @ VGS = 3 V
wv -i ${path}/ID_vs_VDS_swp_VGS/data/NMOS_ID_vs_VDS_para_VGS_3V.csv -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_3V.png -xl '$V_{DS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -yr 0 0.004 -sep $sep -hi 1 $flags
inkscape -D ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_3V.svg -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_3V.pdf --export-latex
# Imágenes ID vs VDS (grupo) @ VGS = 4 V
wv -i ${path}/ID_vs_VDS_swp_VGS/data/NMOS_ID_vs_VDS_para_VGS_4V.csv -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_4V.png -xl '$V_{DS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -yr 0 0.004 -sep $sep -hi 1 $flags
inkscape -D ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_4V.svg -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_4V.pdf --export-latex
# Imágenes ID vs VDS (grupo) @ VGS = 5 V
wv -i ${path}/ID_vs_VDS_swp_VGS/data/NMOS_ID_vs_VDS_para_VGS_5V.csv -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_5V.png -xl '$V_{DS}\,[V]$' -yl '$I_{D}\,[A]$' -lw $line_width -pm --width $width --height $height -ls $labels_size -yr 0 0.004 -sep $sep -hi 1 $flags
inkscape -D ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_5V.svg -o ${path}/ID_vs_VDS_swp_VGS/images/NMOS_ID_vs_VDS_para_VGS_5V.pdf --export-latex
# }}}

# vim:foldmethod=marker:foldlevel=0
