\chapter{Introducción}
  Con la intención de llevar a cabo la realización de mi trabajo final de grado es que se comenzó con la búsqueda de un tema apto para éste. Particularmente, se lo intentó encuadrar dentro de los conceptos A.F.A. (Apto, Factible, Aceptable) siguiendo los lineamientos y metodologías propuestos por la cátedra. De la consulta con los representantes de la cátedra, mis intereses y la aplicación de los anteriores es que se arribó a la elección del tema a tratar.

  Para el desarrollo del trabajo final de grado fue necesario el uso de cierto material bibliográfico, de diverso carácter teórico, así como también manuales de usuario y documentación para algunos de los diversos programas informáticos utilizados para su correcta configuración que, a su vez, permitiera el uso integrado de todas las herramientas de software que más adelante serán detalladas\footnote{Todo el material consultado puede ser encontrado en el capítulo dedicado a la bibliografía. Así mismo, podrán ser encontrados en la sección dedicada a los anexos diversos extractos teóricos (y desarrollos matemáticos), considerados fundamentales para la elaboración del presente trabajo.}.

  Estos últimos fueron de vital importancia ya que facilitaron la información necesaria para la puesta a punto de todos los programas, simplificando el trabajo jerárquico fluido entre las etapas del diseño (siempre en cumplimiento con las restricciones impuestas por el fabricante para el proceso elegido) lo que normalmente se conoce como <<flujo de trabajo>> (del inglés: <<\Gls{workflow}>>).

  \newpage

  \section{Análisis del problema}
    Sería oportuno comenzar la exposición de este trabajo dando una base de fundamentos sobre el principal tema a tratar a lo largo del desarrollo de este: el amplificador operacional.

    Los amplificadores operacionales tienen su origen en la época de las computadoras analógicas, donde eran utilizados para realizar operaciones matemáticas lineales, no lineales y en circuitos dependientes de la frecuencia de operación. Su popularidad como bloque de construcción en circuitos analógicos se debe a su versatilidad. Una de sus características más destacadas es su configuración en lo que se conoce como topología de realimentación negativa, que da como resultado que las características del amplificador operacional en el circuito como: ganancia, impedancia de entrada y salida, ancho de banda, entre otros, quede determinado prácticamente en su totalidad por los componentes externos con los que se configura su operación y muy poca influencia sea debida a factores como podrían ser: el coeficiente de temperatura, las tolerancias del proceso de fabricación y/o de ingeniería internas al amplificador \cite{wikipedia_OpAmp}.

    El amplificador operacional, como su nombre lo indica, es un tipo de amplificador de tipo diferencial (entre otros que existen en esa categoría\footnote{Otros amplificadores que caben en esa categoría pueden ser: el amplificador totalmente diferencial (<<\glsxtrshort{fda}>>, por sus siglas en inglés), el amplificador de instrumentación (generalmente construido mediante tres amplificadores operacionales convencionales), el amplificador de aislación (similar al anterior pero con tolerancia a voltajes de modo común que destruirían la etapa de entrada de los amplificadores comunes), entre otros tipos y configuraciones posibles \cite{wikipedia_OpAmp}.}), puede ser definido como un sistema electrónico con dos entradas y con una salida, cuyo comportamiento típico es la amplificación, obtenida en el puerto de salida, de la diferencia entre la señal, presente entre sus dos entradas.

    En el presente trabajo se realizará el diseño e implementación física de un amplificador operacional, de dos entradas diferenciales, una salida, además de los puertos necesarios para la alimentación y señales de referencia para la correcta calibración del dispositivo.


  \section{Análisis de sistemas existentes} \label{sec:analisis_de_sistemas_existentes}
    En esta sección cabría realizar el análisis de las siguientes cuestiones:
    \begin{enumerate}
      \item \label{uno} Analizar las principales características de algunos amplificadores operacionales ya existentes, comercialmente disponibles, frente a aquellas del presente desarrollo: ¿tienen características similares, se pueden alcanzar las prestaciones que ofrece un diseño comercial?
      \item \label{dos} Dar cuenta de la elección de la tecnología elegida, siendo que existen otras posibles y, a su vez, varios nodos para cada una de ellas: ¿por qué elegir \glsxtrshort{cmos} como tecnología de fabricación?
      \item \label{tres} Justificar la elección de la topología propuesta, siendo que existen otras: ¿por qué elegir dos etapas de amplificación? ¿por qué la elección de una topología de realimentación tipo Miller?
    \end{enumerate}

    A la hora de comparar con dispositivos comerciales, y así tratar de atender la problemática planteada en el \autoref{uno}, puede hacerse mención a algunos de los amplificadores operacionales más conocidos y sus características para entender porque son algunos de los más utilizados e históricamente reconocidos \cite{dave-jones_jellybean-opamps}:

    \begin{table}[htpb]
      \centering
      \resizebox{\textwidth}{!}{
        \begin{tabular}{@{}lccccccccc@{}}
          \input{../recursos/tablas/OpAmps_comerciales.tex}
        \end{tabular}
      }
      \caption{Características básicas de los amplificadores comerciales típicos}
      \label{tab:opamps_comerciales}
    \end{table}
    \todo[inline]{Quizás faltan algunas columnas como power consumption, voltaje de operación y entrada, slew-rate, etc.}

    De todos los ejemplos presentados en la \autoref{tab:opamps_comerciales}, uno en particular es de los más utilizados e históricamente reconocido (introducido de manera comercial en 1968 por Fairchild siendo incluso construido hasta el día de hoy \cite{TI_741}) es el amplificador operacional LM741 (inicialmente llamado µA741) \cite{ieee_741}, \cite{historia_741}\footnote{Puede referirse a un extracto de las hojas de datos para los dispositivos en el \refSectionName{app:delta}, incluido el LM741.}.

    Un amplificador operacional de uso general que ofrece características interesantes que hacen su uso muy simple, cuenta con:
    \begin{itemize}
      \item Tres etapas de amplificación: Diferencial (entrada), Alta Ganancia (intermedia), Salida (salida).
      \item Protección por sobrecarga de entrada y salida y protección de enclavamiento (del inglés: <<\Gls{latch-up}>>).
      \item Capacidad de offset de voltaje nulo.
      \item Puede operar a lazo abierto y cerrado. Posee una excelente estabilidad en lazo abierto sin presentar oscilaciones en el rango de trabajo y sin requerir compensación externa de frecuencia.
      \item Amplio rango de voltaje de operación. Pudiendo además operar con fuentes de alimentación en <<single->> o <<dual-rail>>. Su salida puede prácticamente alcanzar ambos límites de tensión (impuestos por la fuente de alimentación).
      \item Es capaz de realizar todas las operaciones que teóricamente puede realizar un OpAmp.
    \end{itemize}

    Como respuesta al \autoref{dos}, puede decirse que a pesar de existir otras tecnologías con mejor desempeño analógico como: menor umbral de ruido, mayor ancho de banda y ganancia, la motivación del uso de la tecnología \glsxtrshort{cmos} tiene fundamento, entre otros posibles, en los siguientes:
    \begin{itemize}
      \item \textbf{Complejidad del proceso:} es deseable que las etapas que componen el proceso sean acotadas y simples. Cada etapa lleva asociado cierto costo y complejidad, lo que trae por su parte, una mayor probabilidad de fallos que puede derivar en un impacto en el rendimiento final del proceso. El diseño será realizado en un proceso maduro, con cierta antigüedad. Por su parte esto implica que es un proceso simple (transistores planos, con tecnología de fabricación con un número reducido de máscaras), un proceso ya validado y con fines académicos y destinado a aplicaciones más simples, por lo que es extremadamente económico en comparación a nodos más recientes.
      \item \textbf{Área:} por regla general se intenta mantener el área de cada chip lo más acotada posible, hay varios motivos por lo que esto es deseable:
        \begin{itemize}
          \item \textbf{Escalabilidad:} La tecnología \glsxtrshort{mos} permite alcanzar un gran nivel de densidad de dispositivos activos, lo que permite lograr diseños de lo más variado, siendo posible un gran escalado para un mismo nodo antes de tener que pasar al siguiente. 
          \item \textbf{Potencia:} Mayor nivel de integración para la misma área comparado a otras tecnología, lo que implica, a su vez, menor consumo por unidad de área. Además de tener unas características de disipación de energía estática y dinámicas muy pequeñas, lo que les da una ventaja comparativa en desarrollos donde prima el bajo consumo de potencia.
          \item \textbf{Yield:} Los defectos de fabricación se dispersan de manera aleatoria sobre la oblea donde serán fabricados los chips, por lo que chips con una mayor integración permiten tener las mismas características en un área menor, lo que a su vez mejora las probabilidades de fabricarlo correctamente (mayor yield o rendimiento de fabricación).
          \item \textbf{Componentes fabricables:} La tecnología \glsxtrshort{mos} permite fabricar, en forma conjunta a los transistores, otros elementos como resistencias y capacitores. Sin embargo, la magnitud de estos es directamente proporcional al área que precisan para su fabricación, por lo que no es inusual que un capacitor de capacidad considerable ocupe la misma área de chip que todo el diseño de un amplificador o sistema. Debido a esto, la tendencia es a fabricar varios elementos activos por algunos pasivos y siempre y cuando estos últimos sean absolutamente necesarios para el diseño.
        \end{itemize}
      \item \textbf{Integración tecnológica:} Esta tecnología permite realizar integraciones entre diferentes dominios como pueden ser el Analógico-Digital, aplicaciones ópticas, \glsxtrshort{mems}, etc. Esto permite una evolución hacia soluciones del tipo \glsxtrfull{soc}.
      \item \textbf{Adaptación de los parámetros de los dispositivos:} Una de las principales ventajas de la fabricación de dispositivos integrados. Se puede lograr una muy alta precisión en la adaptación de las características de dispositivos activos ya que estos son fabricados todos en simultaneo y, por ende, todos los componentes tienden a sufrir, aproximadamente, las mismas desviaciones del proceso de fabricación.
    \end{itemize}

    Estas características muestran como esta tecnología permite la fabricación de soluciones integrales, de forma monolítica, a un muy bajo costo para sistemas cada vez más complejos \cite[página 426-428]{electronica_Hambley}.

    Ejemplos de lo anterior son chips de sistemas de comunicación como Wi-Fi, 3G/4G/5G, Bluetooth, Ethernet, audio, video, etc.
    La electrónica de consumo, procesamiento y de datos es impulsada por tecnología \glsxtrshort{cmos}. \cite{wikipedia_CMOS}

    Finalmente, para abordar las consultas planteadas en el \autoref{tres} se responderá en el orden en el que las preguntas fueron realizadas:
    \begin{itemize} 
      \item Se eligió una topología de dos etapas de amplificación ya que es el mínimo número de etapas de amplificación en cascada necesarias para alcanzar la magnitud de ganancia planteada en los objetivos del presente trabajo.
      \item Debido a la necesidad de utilizar más de una etapa amplificadora, se hizo indispensable proveer al sistema de una red de realimentación para alcanzar la estabilidad en todo el rango de frecuencias de trabajo. Entre algunas de las posibles técnicas de realimentación como: un capacitor en derivación (del inglés: <<\Gls{shunt-capacitance}>>), un capacitor equivalente Miller o, incluso, otro amplificador operacional, se optó por el segundo método\footnote{Uno de los métodos más utilizado en la actualidad \cite{miller_frequency_compensation}, al mismo tiempo que sencillo de implementar y evitando la incorporación de más componentes activos (y su complejidad asociada). Como contrapartida, el método utiliza un capacitor cuya implementación conlleva un gran porcentaje de área del chip.}.
      \end{itemize}


  \section{Descripción de las actividades del proyecto}

    A continuación, se describen todas las actividades realizadas en el proyecto.

    \begin{itemize}
      \item Elección de las características de funcionamiento del dispositivo deseado
      \item Elección de la tecnología y nodo de fabricación así como el fabricante
      \item Evaluación de la tecnología del nodo de fabricación
      \begin{itemize}
        \item Evaluación de los transistores \glsxtrshort{mos}
        \item Análisis de fuentes de corriente
      \end{itemize}

      \item Simulación fuente de corriente
        \begin{itemize}
          \item Simulación \glsxtrshort{dc}
          % \item Simulación \glsxtrshort{ac}
        \end{itemize}

      \item Especificaciones del amplificador
        \begin{itemize}
          \item Definición de rango de frecuencias de entrada
          \item Definición de ancho de banda
          \item Definición de consumo de energía
          \item Definición de características eléctricas
        \end{itemize}

      \item Definición de jerarquías
        \begin{itemize}
          \item Creación de símbolos jerárquicos
          \item Esquemático
        \end{itemize}

      \item Diseño del amplificador
        \begin{itemize}
          \item Etapa diferencial
          \item Etapa de potencia
          \item Compensación Miller
        \end{itemize}

      \item Simulación del amplificador
        \begin{itemize}
          \item Simulación \glsxtrshort{dc}
          \item Simulación \glsxtrshort{ac}
        \end{itemize}

      \item Layout
        \begin{itemize}
          \item Definición de tamaño de floorplan
          \item Definición de tamaño del módulo del capacitor
          \item Definición de ubicación de puertos
          \item Definición de uso de metales
          % \item Creación de wells para cada dominio de voltaje
          \item Diseño de distribución para apareamiento de transistores
          % \item Dibujado de transistores
        \end{itemize}

      \item Verificación física
        \begin{itemize}
          \item \glsxtrshort{drc}
          \item \glsxtrshort{erc}
          \begin{itemize}
            \item Chequeo de Well y Substrato
            \item Chequeo de Antena
          \end{itemize}
          \item \glsxtrshort{ncc}/\glsxtrshort{lvs}
        \end{itemize}

      \item Extracción de parásitos
        \begin{itemize}
          \item Configuración de extracción conservativa
          \item Anotado de parásitos en netlist
        \end{itemize}

      \item Simulación post-layout
        \begin{itemize}
          \item Simulación \glsxtrshort{ac}
        \end{itemize}
      \item Análisis de potencia de ruido
      \item Análisis de \glsxtrshort{thd}

      \item Generación de archivos de fabricación
        \begin{itemize}
          \item Exportación de \glsxtrshort{gds} stream format
        \end{itemize}
    \end{itemize}
